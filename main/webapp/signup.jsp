<%@ taglib prefix="s" uri="/struts-tags" %>
<html lang="en">
<head>
    <title>SignUp page</title>
    <link rel="stylesheet" href="CSS/signup.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <h1 class="signUp-h1">Registration Form</h1>
    <s:if test="hasActionMessages()">
        <div class="alert alert-info">
            <s:actionmessage />
        </div>
    </s:if>
    <form action="signup" class="signUp-form" method="post">

        <label for="phone_no" >
            Enter Phone No:
        </label>
        <input id="phone_no"  name="phoneNo" type="text" required  placeholder="phone_no" pattern="[0-9]{10}">
        <br>
        <label for="email" >
            Enter Email:
        </label>
        <input id="email" name="email" type="email" required  placeholder="email" >
        <br>
        <label for="name" >
            Enter your Name:
        </label>
        <input id="name"  name="name" type="text" required  placeholder="name" pattern="[A-Za-z]{3,45}">
        <br>
        <label for="gender" >
            Select Gender:
        </label>
        <select name="gender" id="gender" required >
            <option value="0">Male</option>
            <option value="1">Female</option>
            <option value="2">TransGender</option>
        </select>
        <br>
        <label for="password" >
            Enter your password:
        </label>
        <input id="password" name="password" type="password" required  placeholder="password" pattern="[a-zA-Z#@_!]{4,16}">
        <br>
        <div class="addressForm">
            <label for="state">
                State:
            </label>
            <input id="state"  name="state" type="text" required  pattern="[a-zA-Z]{3,25}" placeholder="state" >
            <br>
            <label for="district">
                District:
            </label>
            <input id="district"  name="district" type="text" required pattern="[a-zA-Z]{4,45}" placeholder="district">
            <br>
            <label for="pincode">
                PinCode:
            </label>
            <input id="pincode"  name="pincode" type="text" required pattern="[0-9]{6}" placeholder="pincode">
            <br>
            <label for="locality" style="display: block">
                Locality:
            </label>
            <textarea  name = "locality" maxlength="200"  id="locality" placeholder="Enter your locality"  rows="2" cols="50">

            </textarea>
            <br>

            <input type="submit" id="submit-button" value="SignUp">
        </div>
    </form>

</body>
</html>
