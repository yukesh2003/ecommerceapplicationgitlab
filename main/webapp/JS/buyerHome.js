const products=document.querySelectorAll('.product');
products.forEach(product => {
    const countElement = product.querySelector('.count');
    const count = countElement.textContent;
    const button = product.querySelector('.remove-button');
    const button1=product.querySelector(".buy-now");
    console.log(count);
    button.hidden = parseInt(count) < 1 || count === "";
    button1.hidden = parseInt(count) < 1 || count === "";
})
document.getElementById("previous_button").hidden=((document.getElementById("pageNo").innerText==='1') || (parseInt(document.getElementById("noOfProducts").value.trim())===0));
document.getElementById("next_button").hidden=(parseInt(document.getElementById("noOfProducts").value.trim())<5)




function sendAjaxRequest(pId,counts)
    {
        var xhttp = new XMLHttpRequest();
        var url = "add_to_cart";
        xhttp.open("POST", url,true);
        xhttp.onreadystatechange = function()
        {
            if (this.readyState === 4 && this.status === 200)
            {
                var responseText=this.responseText;
                if(parseInt(responseText)===0)
                {
                    document.getElementById("count"+pId).innerHTML=" ";
                    document.getElementById("buyNow"+pId).hidden=true;
                    document.getElementById("remove-button"+pId).hidden=true;
                }
                if(parseInt(responseText)<parseInt("0"))
                {
                    document.getElementById("remove-button"+pId).hidden=true;
                    document.getElementById("buyNow"+pId).hidden=true;
                    document.getElementById("count"+pId).innerText =responseText;
                }
                if(parseInt(responseText)>parseInt("0"))
                {
                    document.getElementById("remove-button"+pId).hidden=false;
                    document.getElementById("buyNow"+pId).hidden=false;
                    document.getElementById("count"+pId).hidden=false;
                    document.getElementById("count"+pId).innerText =responseText;
                }
                refreshCartPrice();
            }
        };
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send( "productId=" + String(pId) + "&count=" +String(counts));

    }


function refreshCartPrice()
{
        var xhttp = new XMLHttpRequest();
        var url = "refresh_cart_price";
        xhttp.open("POST", url,true);
        xhttp.onreadystatechange = function()
        {
            if (this.readyState === 4 && this.status === 200)
            {
                document.getElementById("checkoutButton").hidden = parseInt(this.responseText) === 0;
                document.querySelector(".buy-now").hidden = parseInt(this.responseText) === 0;
                document.getElementById("cartPrice").innerText = this.responseText;
            }
        }
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send( );
}

