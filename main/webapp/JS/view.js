//disabling manage button for views with no records.
    const manageButton = document.getElementById("manage-button") ;
    const table=document.querySelectorAll("tr");
    if(!(table.length>1))
    manageButton.remove();

 //exclusive for product editing in productSelection.jsp

    const productSelectionForm=document.getElementById("select-edit-product");
    const editProductForm=document.getElementById("edit-product-form");
    editProductForm.hidden=true;
    productSelectionForm.addEventListener("submit",function ()
    {
        editProductForm.hidden=false;
    }
    );