const submit=document.getElementById("submit-button");
submit.preventDefault();
const brand=document.getElementById("brand_name").value;
function addBrand() {
    let x = document.getElementById("brand_name").value;
    if (x .length<3)
    {
        alert("Valid Name must be Entered");
        return false;
    }
    else
    {
        verifyBrand();
    }
}

function verifyBrand()
{
    var xhttp = new XMLHttpRequest();
    var url = "verify_brand";
    xhttp.open("POST", url,true);
    const name=document.getElementById("brand_name").value;

    xhttp.onreadystatechange = function()
    {
        if (this.readyState === 4 && this.status === 200)
        {
           if(this.responseText.trim()==="true")
           {
             addToDb();
           }
            else if(this.responseText.trim()==="false")
           {
              alert("Brand already Exists")
           }
        }
    }
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("name="+name);
}
function addToDb()
{
    var xhttp = new XMLHttpRequest();
    var url = "add_brand";
    xhttp.open("POST", url,true);
    const name=document.getElementById("brand_name").value;
    xhttp.onreadystatechange = function()
    {
        if (this.readyState === 4 && this.status === 200)
        {
            if(this.responseText.trim()==="true")
            {
                alert("Brand added Successfully");
                location='adminHome';
            }
            else if(this.responseText.trim()==="false")
            {
               alert("cannot add brand");
                location='adminHome';
            }

        }

    }
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("name="+name);
}
