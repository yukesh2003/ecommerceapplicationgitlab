const submit=document.getElementById("submit-button");
submit.preventDefault();
const category=document.getElementById("category_name").value;
function addCategory() {
    let x = document.getElementById("category_name").value;
    if (x .length<3)
    {
        alert("Valid Name must be Entered");
        return false;
    }
    else
    {
        verifyCategory();
    }
}

function verifyCategory()
{
    var xhttp = new XMLHttpRequest();
    var url = "verify_category";
    xhttp.open("POST", url,true);
    const name=document.getElementById("category_name").value;

    xhttp.onreadystatechange = function()
    {
        if (this.readyState === 4 && this.status === 200)
        {
            if(this.responseText.trim()==="true")
            {
               addToDb();
            }
            else if(this.responseText.trim()==="false")
            {
                alert("Category already exists")
            }
        }
    }
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("name="+name);
}
function addToDb()
{
    var xhttp = new XMLHttpRequest();
    var url = "add_category";
    xhttp.open("POST", url,true);
    const name=document.getElementById("category_name").value;
    xhttp.onreadystatechange = function()
    {
        if (this.readyState === 4 && this.status === 200)
        {
            if(this.responseText.trim()==="true")
            {
                alert("Category added Successfully");
                location='adminHome';
            }
            else if(this.responseText.trim()==="false")
            {
                alert("cannot add Category");
                location='adminHome.jsp';
            }
        }
    }
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("name="+name);
}
