window.addEventListener("load", function() {
    const rows = document.querySelectorAll(".orderTableBody");
    rows.forEach(row => {
        const status = row.querySelector(".status");
        row.querySelector(".changeButton").hidden = status.innerText.trim() === "DELIVERED";
    });
});



function changeOrderStatus(orderId)
{
    var xhttp = new XMLHttpRequest();
    var url = "change_order_status";
    xhttp.open("POST", url,true);
    xhttp.onreadystatechange = function()
    {
        if (this.readyState === 4 && this.status === 200)
        {
            document.getElementById("status"+orderId).innerText=this.responseText;
            document.getElementById("changeButton"+orderId).hidden=true;
        }
    }
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("orderId="+orderId );
}