<%@ page import="java.sql.SQLException" %>
<%@ page import="com.ecommerce.model.product.Order" %>
<%@ page import="com.ecommerce.model.DbOperations.BeanInstance" %>
<%@ page import="com.ecommerce.model.product.Product" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.ecommerce.model.DbOperations.OrderDAOImpl" %>
<%@ page import="com.ecommerce.model.user.User" %>
<%@ page import="java.util.Set" %>
<%@ page contentType="text/html;charset=UTF-8"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Order View</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/view.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body style="display:flex;justify-content: flex-start">
<% try
{
    Set<Order> orderSet= BeanInstance.getOrderDAOType3().getOrders(((User)request.getSession().getAttribute("user")).getUserId());
    request.setAttribute("orderSet",orderSet);
    Map<Integer, Product> productMap= BeanInstance.getProductDAOType3().getProductMap();
    request.setAttribute("productMap",productMap);
}
catch (SQLException e) {
    response.getWriter().println(e.getMessage());
}%>
    <table class="view-table" id="view-table">
        <tr class="view-table-row" id="view-table-row-heading">
            <th>order Id</th>
            <th>Product Id</th>
            <th>Count</th>
            <th>orderStatus</th>
        </tr>
    <c:forEach var="order" items="${orderSet}">

        <tr class="orderTableBody">
            <c:forEach var="item" items="${order.itemsCount}">
                <td>${order.orderId}</td>
                <td class="product" >${productMap.get(item.key).name}</td>
                <td class="count" >${item.value}</td>
                <th class="orderStatus">${order.orderStatus} </th>
            </c:forEach>
        </tr>
    </c:forEach>
</table>
<button  type="button" onclick="window.location.href='sellerHome'">Back</button>
<script src="../JS/changeOrderStatus.js"></script>
</body>
</html>
