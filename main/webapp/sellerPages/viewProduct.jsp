<%@ page import="java.sql.SQLException" %>
<%@ page import="com.ecommerce.model.product.Product" %>
<%@ page import="com.ecommerce.model.DbOperations.BeanInstance" %>
<%@ page import="com.ecommerce.model.user.User" %>
<%@ page import="java.util.Set" %>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <title>Product view</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/view.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <% try
    {
       Set<Product> productSet= BeanInstance.getProductDAOType3().getProducts(((User) session.getAttribute("user")).getUserId());
       request.setAttribute("productSet",productSet);
    }
    catch (SQLException e) {
        response.getWriter().println(e.getMessage());
    }%>
    <h1>Seller List</h1>
    <div class="view-options">

        <input type="button" value="Back"  name="back-button" id="back-button" onclick="window.location.href='sellerHome'">
        <input type="button" value="manage" name="manage-button" id="manage-button" onclick="window.location.href='productSelection'" >
    </div>
    <table class="view-table" id="view-table">
        <tr class="view-table-row" id="view-table-row-heading">
            <th>Product Id</th>
            <th>Name</th>
            <th>Price</th>
            <th>Category</th>
            <th>Brand</th>
        </tr>
        <c:forEach var="product" items="${productSet}">
            <tr>
                <td>${product.productId}</td>
                <td>${product.name}</td>
                <td>${product.price}</td>
                <td>${product.brand}</td>
                <td>${product.category}</td>
                <td style="background: none;border: none"></td>
            </tr>
        </c:forEach>
    </table>
    <script src="${pageContext.request.contextPath}/JS/view.js"></script>
</body>
</html>