<%@ page import="java.sql.SQLException" %>
<%@ page import="com.ecommerce.model.product.Product" %>
<%@ page import="com.ecommerce.model.DbOperations.BeanInstance" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%
    int productId=Integer.parseInt(request.getParameter("product"));
    try {
        Product product= BeanInstance.getProductDAOType3().getProduct(productId);
        request.setAttribute("product",product);
    } catch (SQLException e)
    {
        response.getWriter().println(e.getMessage());
    }
%>
<html>
<head>
    <title>Remove User</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/view.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<h2>Edit Product Page</h2>
<form action="remove_product" method="post" class="view-options">
    <input type="hidden" value="${product.productId}" name="productId">
    <input type="submit" value="Remove Product">
</form>

<form class="view-form" method="post" action="update_product" id="edit-product-form" >
    <label for="product_id">Product Id</label>
    <input type="text" id="product_id" name="productId" value="${product.productId}" readonly>
    <label for="product_name" >
        product name:
    </label>
    <input id="product_name"  name="name" type="text" required  value="${product.name}" pattern="[A-Za-z]{3,45}">
    <br>
    <label for="brand_name" >
       brand name:
    </label>
    <input id="brand_name"  name="brand" type="text" required  value="${product.brand}" pattern="[A-Za-z]{3,45}">
    <br>
    <label for="category_name" >
       category name:
    </label>
    <input id="category_name"  name="category" type="text" required  value="${product.category}" pattern="[A-Za-z]{3,45}">
    <br>
    <label for="price" >
       price:
    </label>
    <input id="price"  name="price" type="number" required  value="${product.price}" pattern="[0-9]">
    <br>
    <label for="color">
        color:
    </label>
    <input id="color" name="color" type="text" value="${product.description.color}"  >
    <br>

    <label for="size">
        size:
    </label>
    <input id="size" name="size" type="number" value="${product.description.size}"  >
    <br>
    <label for="idealFor">
        ideal For/Customer Group:
    </label>
    <input id="idealFor" name="idealFor" type="text" value="${product.description.customerGroup}" pattern="[A-Za-z]{3,45}" >
    <br>
    <input type="submit" id="submit-button" value="Make changes">

</form>

</body>
</html>