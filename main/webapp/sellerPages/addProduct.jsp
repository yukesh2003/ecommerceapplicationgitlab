<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en">
<head>
    <title>Add Category</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/addSeller.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<h1 class="add-seller-h1">Add Product</h1>
<s:if test="hasActionMessages()">
    <div class="alert alert-info">
        <s:actionmessage />
    </div>
</s:if>

<form class="add-seller-form"  action="add_product" method="post" enctype="multipart/form-data">

    <label for="product_name" >
        Enter product name:
    </label>
    <input id="product_name"  name="name" type="text" required  placeholder="product_name" pattern="[A-Za-z]{3,45}">
    <br>
    <label for="brand_name" >
        Enter brand name:
    </label>
    <input id="brand_name"  name="brand" type="text" required  placeholder="brand_name" pattern="[A-Za-z]{3,45}">
    <br>
    <label for="category_name" >
        Enter category name:
    </label>
    <input id="category_name"  name="category" type="text" required  placeholder="category_name" pattern="[A-Za-z]{3,45}">
    <br>
    <label for="price" >
        Enter price:
    </label>
    <input id="price"  name="price" type="number" required  placeholder="price" pattern="[0-9]">
    <br>

    <label for="color">
        color:
    </label>
    <input id="color" name="color" type="text" value="not entered"  >
    <br>

    <label for="size">
        size:
    </label>
    <input id="size" name="size" type="number" value="0"  >
    <br>
    <label for="idealFor">
        ideal For/Customer Group:
    </label>
    <input id="idealFor" name="idealFor" type="text" value="not entered"  >
    <br>
    <input type="file"  name="image" id="image" accept="image/*" required >
    <input type="submit" id="submit-button" value="Add Product">
    <br>
    <input type="button" value="Back" onclick="window.location.href='sellerHome'">
</form>
</body>
</html>