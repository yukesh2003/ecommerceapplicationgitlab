<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<script type = "text/javascript" >
    function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
</script>
<!DOCTYPE html>
<html>
<head>
    <title>Seller Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/sellerHome.css">
</head>
<body>
<div class="h2-div">
    <h2 class="home-h2">First Choice-India :Seller Page</h2>
    <s:if test="hasActionMessages()">
        <div class="alert alert-info">
            <s:actionmessage />
        </div>
    </s:if>
</div>

<div class="home-options">
    <div class="bg-img">
        <img src="${pageContext.request.contextPath}/img/seller.png" alt="bg image">
    </div>
    <div class="button-options">
    <button class="options-buttons" id="view-orders-button" onclick="window.location.href='sellerOrder'">
        View  Orders
    </button>
    <button class="options-buttons" id="add-product-button" onclick="window.location.href='addProduct'">
        Add Product
    </button>
    <button class="options-buttons" id="view-product-button" onclick="window.location.href='viewProduct'">
        View Products
    </button>

    <form action="signout" method="post">
        <button class="options-buttons" type="submit" id="sign-out-button" >
            Sign Out
        </button>
    </form>
</div>
</div>
</body>
</html>
