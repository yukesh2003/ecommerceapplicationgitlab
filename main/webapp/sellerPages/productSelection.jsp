<%@ page import="java.sql.SQLException" %>
<%@ page import="com.ecommerce.model.product.Product" %>
<%@ page import="com.ecommerce.model.DbOperations.BeanInstance" %>
<%@ page import="com.ecommerce.model.user.User" %>
<%@ page import="java.util.Set" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Remove User</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/view.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<%
    try
    {
        int userId= ((User) request.getSession().getAttribute("user")).getUserId();
        Set<Product> productSet =BeanInstance.getProductDAOType3().getProducts(userId);
        request.setAttribute("productSet",productSet);
    }
    catch (SQLException e)
    {
        response.getWriter().println(e.getMessage());
    }
%>
<h1>Edit Product</h1>

<form class="view-table" id="select-edit-product" action="editProduct.jsp" method="post">
    <label for="select-product-dropdown">Select the Product to Edit:</label>
    <select name="product" class="view-table-row" id="select-product-dropdown" required>
        <c:forEach var="product" items="${productSet}">
            <option  value="${product.productId}">${product.name}</option>
        </c:forEach>
    </select>
    <input   type="submit" value="edit" name="submit-button" id="edit-product-button">
</form>
</body>
</html>