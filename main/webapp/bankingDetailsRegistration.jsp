<%
  response.setHeader("Cache-Control", "no-cache");
  response.setHeader("Cache-Control","must-revalidate");
  response.setHeader("Cache-Control", "no-store");
  response.setDateHeader("Expires", 0);
  response.setHeader("Pragma", "no-cache");
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Banking Details Registration</title>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/addSeller.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <h1 class="add-seller-h1">Banking Details Registration  Form</h1>
  <form action="register_bank_details" method="post" class="add-seller-form">
  <label for="accountNo">
    AccountNo:
  </label>
  <input id="accountNo" maxlength="12" minlength="10" name="accountNo" type="text"  pattern="0-9" placeholder="Account Number">
  <br>
  <label for="ifsc">
    IFSC:
  </label>
  <input id="ifsc" maxlength="12" minlength="12" name="ifsc" type="text"   placeholder="IFSC">
  <br>
  <label for="bankName">
    Bank Name:
  </label>
  <input id="bankName" maxlength="20" minlength="10" name="bankName" type="text"  pattern="a-zA-Z" placeholder="Bank Name">
  <br>
    <input type="submit" id="submit-button" value="Register">
  </form>

</body>
</html>