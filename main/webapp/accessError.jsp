<%@ page import="com.ecommerce.model.util.Enums" %>
<%@ page import="com.ecommerce.model.user.User" %>
<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Cache-Control","must-revalidate");
    response.setHeader("Cache-Control", "no-store");
    response.setDateHeader("Expires", 0);
    response.setHeader("Pragma", "no-cache");
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Access denied</title>
</head>
<body>
  <%  if(request.getSession().getAttribute("user")==null)
        {
            response.sendRedirect("login.jsp");
        }
      else
      {
          Enums.UserType userType =  ((User)request.getSession().getAttribute("user")).getUserType();

          if(userType.equals(Enums.UserType.BUYER))
              request.getRequestDispatcher("buyerHome").forward(request,response);
          if(userType.equals(Enums.UserType.SELLER))
              request.getRequestDispatcher("sellerHome").forward(request,response);
          if(userType.equals(Enums.UserType.DELIVERY_AGENT))
              request.getRequestDispatcher("deliveryAgentHome").forward(request,response);
          if(userType.equals(Enums.UserType.ADMINISTRATOR))
              request.getRequestDispatcher("adminHome").forward(request,response);

      }
  %>
</body>
</html>