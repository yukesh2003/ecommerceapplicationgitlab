<%@ page contentType="text/html;charset=UTF-8" %>
<script type = "text/javascript" >
    function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
</script>
<!DOCTYPE html>
<html>
<head>
    <title>Seller Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/home.css">
</head>
<body>
<h2 class="home-h2">First Choice-India :Delivery Agent Page</h2>
<div class="home-options">
    <button class="options-buttons" id="manage-orders-button" onclick="window.location.href='deliveryAgentOrder'">
        Manage  Orders
    </button>

    <form action="signout" method="post">
        <button class="options-buttons" type="submit" id="sign-out-button" >
            Sign Out
        </button>
    </form>
</div>
</body>
</html>
