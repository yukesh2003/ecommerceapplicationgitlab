<%@ page import="com.ecommerce.model.DbOperations.BeanInstance" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.ecommerce.model.user.User" %>
<%@ page import="com.ecommerce.model.product.Order" %>
<%@ page import="java.util.Set" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Order View</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/view.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div id="deliveryAgentDiv">
<% try
{
    Set<Order> orderSet= BeanInstance.getOrderDAOType3().getOrders(((User) request.getSession().getAttribute("user")).getUserId());
    request.setAttribute("orderSet",orderSet);
}
catch (SQLException e) {
    response.getWriter().println(e.getMessage());
}%>
    <table class="view-table" id="view-table">
    <tr class="view-table-row" id="view-table-row-heading">
        <th>Order Id</th>
        <th>Status</th>
    </tr>
<c:forEach var="order" items="${orderSet}">
    <tr class="orderTableBody">
        <td>${order.orderId}</td>
        <td class="status" id="status${order.orderId}">${order.orderStatus}</td>
        <td><button class="changeButton" id="changeButton${order.orderId}"  onclick="changeOrderStatus(${order.orderId})">Change</button></td>
    </tr>
</c:forEach>
</table>
<button type="button" onclick="window.location.href='deliveryAgentHome'">Back</button>
</div>
<script src="${pageContext.request.contextPath}/JS/changeOrderStatus.js"></script>
</body>
</html>
