<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html lang="en">
<head>
    <title>Add Brand</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/addSeller.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <h1 class="add-seller-h1">Add Brand</h1>
    <div  class="add-seller-form">
        <label for="brand_name" >
            Enter Brand name:
        </label>
        <input id="brand_name"  name="brandName" type="text" required  placeholder="brand_name" pattern="[A-Za-z]{3,45}">
        <br>
        <div class="buttons">
            <button  id="submit-button" onclick="addBrand()">Add Brand</button>
            <input type="button" value="Back" onclick="window.location.href='adminHome'">
        </div>
    </div>
    <script src="${pageContext.request.contextPath}/JS/addBrand.js"></script>
</body>
</html>