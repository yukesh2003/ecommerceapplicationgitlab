<%@ page import="java.sql.SQLException" %>
<%@ page import="com.ecommerce.model.user.DeliveryAgent" %>
<%@ page import="com.ecommerce.model.DbOperations.BeanInstance" %>
<%@ page import="java.util.Set" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Remove User</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/view.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<%
    try
    {
        Set<DeliveryAgent> deliveryAgentList = BeanInstance.getUserDAOType3().getDeliveryAgents();
        request.setAttribute("DeliveryAgentList",deliveryAgentList);
    }
    catch (SQLException e)
    {
        response.getWriter().println(e.getMessage());
    }
%>
<h1>RemoveSeller</h1>

<form action="remove_user" class="view-table" method="post">
    <label for="select-user-dropdown">Select the Delivery Agent to delete:</label>
    <select name="user" class="view-table-row" id="select-user-dropdown">
        <c:forEach var="deliveryAgent" items="${DeliveryAgentList}">
            <option value="${deliveryAgent.userId}" >${deliveryAgent.name}</option>
        </c:forEach>
    </select>
    <input type="submit" value="remove" name="submit-button">
</form>
</body>
</html>