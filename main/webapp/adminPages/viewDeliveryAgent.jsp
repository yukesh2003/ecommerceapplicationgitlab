<%@ page import="java.sql.SQLException" %>
<%@ page import="com.ecommerce.model.user.DeliveryAgent" %>
<%@ page import="com.ecommerce.model.DbOperations.BeanInstance" %>
<%@ page import="java.util.Set" %>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <title>Add Seller</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/view.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

    <h1>Delivery Agent List</h1>
    <div class="view-options">

        <input type="button" value="Back"  name="back-button" id="back-button" onclick="window.location.href='adminHome'">
        <input type="button" value="manage" name="manage-button" id="manage-button" onclick="window.location.href='removeDeliveryAgent'">
    </div>
    <table class="view-table" id="view-table">
    <tr class="view-table-row" id="view-table-row-heading">
        <th>User Id</th>
        <th>Name</th>
        <th>Email</th>
        <th>phone</th>
        <th>Locality</th>
        <th>District</th>
        <th>State</th>
        <th>Pincode</th>
    </tr>
    <%
        try {
            Set<DeliveryAgent> deliveryAgentSet = BeanInstance.getUserDAOType3().getDeliveryAgents();
            request.setAttribute("deliveryAgentSet",deliveryAgentSet);
        }
        catch (SQLException e) {
            response.getWriter().println(e.getMessage());
        }
    %>

    <c:forEach var="deliveryAgent" items="${deliveryAgentSet}">
        <tr>
            <td>${deliveryAgent.userId}</td>
            <td>${deliveryAgent.name}</td>
            <td>${deliveryAgent.email}</td>
            <td>${deliveryAgent.phone}</td>
            <td>${deliveryAgent.address.locality}</td>
            <td>${deliveryAgent.address.district}</td>
            <td>${deliveryAgent.address.state}</td>
            <td>${deliveryAgent.address.pinCode}</td>
            <td>${deliveryAgent.status}</td>

        </tr>
        </c:forEach>
    </table>
    <script src="${pageContext.request.contextPath}/JS/view.js"></script>
</body>
</html>
