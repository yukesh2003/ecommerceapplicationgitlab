<%@ page import="com.ecommerce.model.user.Seller" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.util.Set" %>
<%@ page import="com.ecommerce.model.DbOperations.BeanInstance" %>
<%@ page contentType="text/html;charset=UTF-8"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="en">
<head>
    <title>View Seller</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/view.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <h1>Seller List</h1>
    <div class="view-options">

        <input type="button" value="Back"  name="back-button" id="back-button" onclick="window.location.href='adminHome'">
        <input type="button" value="manage" name="manage-button" id="manage-button" onclick="window.location.href='removeSeller'">
    </div>
    <table class="view-table" id="view-table">
        <tr class="view-table-heading" id="view-table-row-heading">
            <th>User Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>phone</th>
            <th>Locality</th>
            <th>District</th>
            <th>State</th>
            <th>Pincode</th>
        </tr>
        <%
            try {
                Set<Seller> sellerSet = BeanInstance.getUserDAOType3().getSellers();
                request.setAttribute("sellerSet",sellerSet);
            } catch (SQLException e)
            {
                response.getWriter().println(e.getMessage());
            }
        %>

        <c:forEach var="seller" items="${sellerSet}">
        <tr class="view-table-row">
            <td>${seller.userId}</td>
            <td>${seller.name}</td>
            <td>${seller.email}</td>
            <td>${seller.phone}</td>
            <td>${seller.address.locality}</td>
            <td>${seller.address.district}</td>
            <td>${seller.address.state}</td>
            <td>${seller.address.pinCode}</td>
            <td>${seller.status}</td>

        </tr>
        </c:forEach>
    </table>
    <script src="${pageContext.request.contextPath}/JS/view.js"></script>
</body>
</html>
