<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="java.util.logging.Logger" %>
<%@ page import="java.util.logging.Level" %>
<%@ page contentType="text/html;charset=UTF-8"  %>
<script type = "text/javascript" >
    function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
</script>
<% Logger.getAnonymousLogger().log(Level.SEVERE,"INSIDE JSP FILE");%>
<!DOCTYPE html>
<html>
<head>
    <title>Admin Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/home.css">
</head>
<body>
<h2 class="home-h2">First Choice-India :Administrator Page</h2>
<div class="add-seller-form" >
<div class="home-options">
    <button class="options-buttons" id="view-seller-button" onclick="window.location.href='viewSeller'">
        View  Sellers
    </button>
    <button class="options-buttons" id="add-seller-button" onclick="window.location.href='addSeller'">
        Add  Seller
    </button>
    <button class="options-buttons" id="delivery-agent-add-button" onclick="window.location.href='addDeliveryAgent'">
        Add Delivery Agent
    </button>
    <button class="options-buttons" id="delivery-agent-view-button" onclick="window.location.href='viewDeliveryAgent'">
        View Delivery Agents
    </button>
    <button class="options-buttons" id="Brand-view-button" onclick="window.location.href='viewBrand'">
        View Brands
    </button>
    <button class="options-buttons" id="Category-view-button" onclick="window.location.href='viewCategory'">
        View Category
    </button>
    <button class="options-buttons" id="add-category-button" onclick="window.location.href='addCategory'">
        Add Category
    </button>
    <button class="options-buttons" id="add-brand-button" onclick="window.location.href='addBrand'">
        Add Brand
    </button>
    <form action="signout" method="post">
        <button class="options-buttons" type="submit" id="Signout-button" onclick="/signout">
            Sign Out
        </button>
    </form>
</div>
</body>
</html>