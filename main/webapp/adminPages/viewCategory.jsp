<%@ page import="com.ecommerce.model.product.Category" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.ecommerce.model.DbOperations.BeanInstance" %>
<%@ page import="java.util.Set" %>
<%@ page contentType="text/html;charset=UTF-8"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Category</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/view.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

<h1>Category List</h1>
<div class="view-options">

    <input type="button" value="Back"  name="back-button" id="back-button" onclick="window.location.href='adminHome'">
    <%--        <input type="button" value="manage" name="manage-button" id="manage-button" onclick="window.location.href='removeDeliveryAgent.jsp'">--%>
</div>
<table class="view-table" id="view-table">
    <tr class="view-table-row" id="view-table-row-heading">
        <th>Category Id</th>
        <th>Name</th>
    </tr>
    <%
        try
        {
            Set<Category> categorySet = BeanInstance.getCategoryDAOType3().getCategories();
            request.setAttribute("categorySet",categorySet);
        }
        catch (SQLException e)
        {
            response.getWriter().println(e.getMessage());
        }
    %>
    <c:forEach var="category" items="${categorySet}">
        <tr>
            <td>${category.categoryId}</td>
            <td>${category.name}</td>
        </tr>
    </c:forEach>
</table>
<script src="${pageContext.request.contextPath}/JS/view.js"></script>
</body>
</html>
