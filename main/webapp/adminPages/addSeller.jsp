<%@ page contentType="text/html;charset=UTF-8"  %>
<html lang="en">
<head>
    <title>Add Seller</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/addSeller.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
  <h1 class="add-seller-h1">Add Seller</h1>

  <form action="add_seller" class="add-seller-form" method="post">

    <label for="phone" >
      Enter Phone No:
    </label>
    <input id="phone"  name="phone" type="text" required  placeholder="phone" pattern="[0-9]{10}">
    <br>
    <label for="email" >
      Enter Email:
    </label>
    <input id="email" name="email" type="email" required  placeholder="email" >
    <br>
    <label for="name" >
      Enter your Business Name:
    </label>
    <input id="name"  name="name" type="text" required  placeholder="name" pattern="[A-Za-z]{3,45}">
    <br>
    <br>
    <label for="password" >
      Enter your password:
    </label>
    <input id="password" name="password" type="password" required  placeholder="password" pattern="[a-zA-Z#@_!]{4,16}">
    <br>
    <div class="addressForm">
      <label for="state">
        State:
      </label>
      <input id="state"  name="state" type="text" required  pattern="[a-zA-Z]{3,25}" placeholder="state" >
      <br>
      <label for="district">
        District:
      </label>
      <input id="district"  name="district" type="text" required pattern="[a-zA-Z]{4,45}" placeholder="district">
      <br>
      <label for="pincode">
        PinCode:
      </label>
      <input id="pincode"  name="pincode" type="text" required pattern="[0-9]{6}" placeholder="pincode">
      <br>
      <label for="locality" style="display: block">
        Locality:
      </label>
      <textarea  name = "locality" maxlength="200"  id="locality" placeholder="Enter your locality"  rows="2" cols="50">
      </textarea>
      <br>
      <input type="submit" id="submit-button" value="Add Seller">
      <br>
      <input type="button" value="Back" onclick="window.location.href='adminHome'">
    </div>
  </form>

</body>
</html>
