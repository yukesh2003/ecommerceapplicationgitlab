<%@ page import="com.ecommerce.model.product.Brand" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.ecommerce.model.DbOperations.BrandDAOImpl" %>
<%@ page import="com.ecommerce.model.DbOperations.BeanInstance" %>
<%@ page import="java.util.Set" %>
<%@ page contentType="text/html;charset=UTF-8"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Brand</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/view.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

    <h1>Brand List</h1>
    <div class="view-options">

        <input type="button" value="Back"  name="back-button" id="back-button" onclick="window.location.href='adminHome'">
<%--        <input type="button" value="manage" name="manage-button" id="manage-button" onclick="window.location.href='removeDeliveryAgent.jsp'">--%>
    </div>
    <table class="view-table" id="view-table">
        <tr class="view-table-row" id="view-table-row-heading">
            <th>Brand Id</th>
            <th>Name</th>
        </tr>
            <%
            try
            {
                Set<Brand> brandSet = BeanInstance.getBrandDAOType1().getBrands();
                request.setAttribute("brandSet",brandSet);
            }
            catch (SQLException e)
            {
                response.getWriter().println(e.getMessage());
            }
            %>
        <c:forEach var="brand" items="${brandSet}">
        <tr>
            <td>${brand.brandId}</td>
            <td>${brand.name}</td>
        </tr>
        </c:forEach>
    </table>
    <script src="${pageContext.request.contextPath}/JS/view.js"></script>
</body>
</html>
