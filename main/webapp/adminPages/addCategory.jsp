<%@ page contentType="text/html;charset=UTF-8"%>
<html lang="en">
<head>
    <title>Add Category</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/addSeller.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <h1 class="add-seller-h1">Add Category</h1>

    <div  class="add-seller-form">
        <label for="category_name" >
            Enter category name:
         </label>
        <input id="category_name"  name="categoryName" type="text" required  placeholder="category_name" pattern="[A-Za-z]{3,45}">
        <br>
        <div class="buttons">
            <button  id="submit-button" onclick="addCategory()">Add category</button>
        <input type="button" value="Back" onclick="window.location.href='adminHome'">
        </div>
    </div>
    <script src="${pageContext.request.contextPath}/JS/addCategory.js"></script>
</body>
</html>
