<%@ page import="com.ecommerce.model.user.Seller" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.util.Set" %>
<%@ page import="com.ecommerce.model.DbOperations.BeanInstance" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8"  %>
<html>
    <head>
        <title>Remove User</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/view.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
<body>
        <%
            try
            {
                Set<Seller> sellerSet = BeanInstance.getUserDAOType3.getSellers();
                request.setAttribute("sellerSet",sellerSet);
            }
            catch (SQLException e)
            {
                response.getWriter().println(e.getMessage());
            }
        %>
        <h1>RemoveSeller</h1>

        <form action="remove_user" class="view-table" method="post">
            <label for="select-user-dropdown">Select the seller to delete:</label>
            <select name="user" class="view-table-row" id="select-user-dropdown">
                <c:forEach var="seller" items="${sellerSet}">
                    <option  value="${seller.userId}">${seller.name}</option>
                </c:forEach>
            </select>
            <input type="submit" value="remove" name="submit-button">
        </form>
</body>
</html>