<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login Page</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/login.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type = "text/javascript" >
        function preventBack(){window.history.forward();}
        setTimeout("preventBack()", 0);
        window.onunload=function(){null};
    </script>
</head>
<body>
<h1 id="login-h1"> Login to Continue</h1>
<div class="add-seller-form" >
    <s:if test="hasActionMessages()">
    <div class="alert alert-info">
        <s:actionmessage />
    </div>
    </s:if>
<br>
<form action="login" name="login-form" id="login-form" method="post">
<div class="login-form">

            <label for="username" >
                 Enter Username
            </label>
             <input id="username" name="userName" type="email" required  placeholder="email">
             <br>

            <label for="password">
                Enter Password
            </label>
             <input id="password" name="password" type="password" required  placeholder="password" pattern="[a-zA-Z#@_!]{4,16}">
            <br>

            <input type="submit" id="submit-button" value="Login">
</div>
</form>
</body>
</html>
