<%@ page import="com.ecommerce.model.DbOperations.BeanInstance" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.ecommerce.model.DbOperations.CartDAOImpl" %>
<%@ page import="com.ecommerce.model.product.Product" %>
<%@ page import="java.util.logging.Logger" %>
<%@ page import="java.util.logging.Level" %>
<%@ page import="com.ecommerce.model.DbOperations.ProductDAOImpl" %>
<%@ page import="com.ecommerce.model.user.Buyer" %>
<%@ page import="java.util.Set" %>
<%@ page import="com.ecommerce.model.util.FilterCriteria" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.ecommerce.model.util.Enums" %>
<%@ page contentType="text/html;charset=UTF-8"  %>
<%
    int productId=Integer.parseInt(request.getParameter("productId"));
    try {
        request.setAttribute("product", BeanInstance.getProductDAOType3().getProduct(productId));
    } catch (SQLException e) {
        throw new RuntimeException(e);
    };

%>
 <%
        try
        {
            request.setAttribute("user",request.getSession().getAttribute("user"));
            request.setAttribute("cartItems",BeanInstance.getCartDAOType3().getCart(((Buyer)request.getSession().getAttribute("user")).getUserId()).getItems());
        }catch (SQLException|NullPointerException e)
        {
            Logger.getAnonymousLogger().log(Level.SEVERE,e.getMessage()+" at buyer Home.jsp");
//             throw new RuntimeException(e);
        }
        %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/buyer.css">
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <title>${product.name}</title>
</head>
<body>
<div class="product" id="p+${product.productId}">
    <img class="product-image" src="data:image/jpeg;base64,${Product.displayImage(BeanInstance.getProductDAOType3().getImagePath(product.productId))}" id="${product.productId}" alt="productImage">
    <div class="product-button" >
        <button id="add-button${product.productId}" class="add_button" onclick="sendAjaxRequest(${product.productId},1)">+</button>
        <p id="count${product.productId}" class="count">${cartItems.get(product.productId)}</p>
        <button  id="remove-button${product.productId}" class="remove-button"  onclick="sendAjaxRequest(${product.productId},-1)">-</button>

        <form action="buy_now" method="post">
            <input type="hidden" name="productId" value="${product.productId}">
            <input type="submit" id="buyNow${product.productId}"  class="buy-now"  value="Buy Now">
        </form>
        <button  onclick="window.location.href='buyerHome'">BACK</button>
    </div>
    <div class="product-text">
        <p class="product-name">${product.name}</p>
        <br>
        <p class="product-brand">Brand:${product.brand}</p><br>
        <p class="product-category">Category:${product.category}</p><br>
        <p class="product-price">Price:$${product.price}</p><br>
        <p class="product-description-size">Size:${product.description.size}</p><br>
        <p class="product-description-color">color:${product.description.color}</p><br>
        <p class="product-description-idealFor">IdealFor:${product.description.customerGroup}</p><br>
    </div>
</div>
<script src="${pageContext.request.contextPath}/JS/productDescription.js"></script>
</body>
</html>
