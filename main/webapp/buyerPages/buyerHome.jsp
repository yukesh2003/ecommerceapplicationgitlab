<%@page import="com.ecommerce.model.DbOperations.BeanInstance" %>
<%@ page import="com.ecommerce.model.product.Product" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.util.logging.Logger" %>
<%@ page import="java.util.logging.Level" %>
<%@ page import="com.ecommerce.model.DbOperations.ProductDAOImpl" %>
<%@ page import="com.ecommerce.model.user.Buyer" %>
<%@ page import="java.util.Set" %>
<%@ page import="com.ecommerce.model.util.FilterCriteria" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.ecommerce.model.util.Enums" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8"  %>


<script type = "text/javascript" >
    function preventBack()
    {
        window.history.forward();
    }
    setTimeout("preventBack()", 0);
    window.onunload=function(){ null };
</script>
<%
//     FilterCriteria.FilterType filterType= FilterCriteria.FilterType.NAME;
//     String value="%%";
//     if(request.getSession().getAttribute("option")!=null)
//     {
//         if(request.getSession().getAttribute("option").equals("name"))
//         {
//         }
//         else if (request.getSession().getAttribute("option").equals("brand"))
//         {
//             filterType= FilterCriteria.FilterType.BRAND;
//         } else if (request.getSession().getAttribute("option").equals("category"))
//         {
//             filterType= FilterCriteria.FilterType.CATEGORY;
//         }
//     }
//     if(request.getSession().getAttribute("text")!=null)
//     {
//         value= "%"+request.getSession().getAttribute("text").toString()+"%";
//     }
//     FilterCriteria filterCriteria= new FilterCriteria(filterType,value);
//     int pageNo=0;
//     if(request.getParameter("page")!=null)
//     {
//         pageNo=Integer.parseInt(request.getParameter("page"));
//     }
//     request.setAttribute("page",pageNo);
//     Enums.Navigation navOption= Enums.Navigation.NEXT;
//     int offset =0;
//     if(request.getParameter("navOption")!=null)
//     {
//         if(request.getParameter("navOption").equals("next"))
//         {
//             offset=Integer.parseInt(request.getParameter("offset"));
//         }
//         if(request.getParameter("navOption").equals("previous"))
//         {
//             navOption= Enums.Navigation.PREVIOUS;
//             offset=Integer.parseInt(request.getParameter("offset"));
//         }
//     }
    Set<Product> productSet;
    try
    {
        productSet= BeanInstance.getProductDAOType3().getProducts();

    }
    catch (SQLException e)
    {
        throw new RuntimeException(e);
    }
    request.setAttribute("productSet",productSet);
    List<Product> products = new ArrayList<>(productSet);
    request.setAttribute("noOfProducts",products.size());
    try
    {
        request.setAttribute("firstProduct", products.get(0).getProductId());
        request.setAttribute("lastProduct", products.get(products.size()-1).getProductId());
    }
    catch (Exception e)
    {
        out.println( "<div class='alert' style=\"color: crimson;background-color: azure;align-items: center;display: block;align-self:center;cursor: pointer;font-size:150%;\"> ");
        out.println("<span class='closebtn' onclick='this.parentElement.style.display=\"none\";'>&times;</span>" );
        out.println("<strong>OOPS!</strong> .....No matches found......");
        out.println("</div> ");
    }
%>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/buyer.css">
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function()
            {
            $("#header").load("header.html");
            $("#footer").load("footer.html");
             }
        );
        <%
        try
        {
            request.setAttribute("user",request.getSession().getAttribute("user"));
            request.setAttribute("cartItems",BeanInstance.getCartDAOType3().getCart(((Buyer)request.getSession().getAttribute("user")).getUserId()).getItems());
        }catch (SQLException|NullPointerException e)
        {
            Logger.getAnonymousLogger().log(Level.SEVERE,e.getMessage()+" at buyer Home.jsp");
//             throw new RuntimeException(e);
        }
        %>

    </script>

    <title>BUYER HOME</title>
</head>
<body>
    <div id="header"></div>

    <div class="product-list">

        <c:forEach var="product" items="${productSet}">
            <div class="product" id="p+${product.productId}">
                <img class="product-image" src="data:image/jpeg;base64,${Product.displayImage(product.imagePath)}" id="${product.productId}" alt="productImage">
                <div class="product-button" >
                    <button id="add-button${product.productId}" class="add_button" onclick="sendAjaxRequest(${product.productId},1)">+</button>
                    <p id="count${product.productId}" class="count">${cartItems.get(product.productId)}</p>
                    <button  id="remove-button${product.productId}" class="remove-button"  onclick="sendAjaxRequest(${product.productId},-1)">-</button>

                    <form action="buy_now" method="post">
                        <input type="hidden" name="productId" value="${product.productId}">
                        <input type="submit" id="buyNow${product.productId}" class="buy-now"  value="Buy Now">
                    </form>
                </div>
                <div class="product-text">
                    <p class="product-name">${product.name}</p>
                    <br>
                     <p class="product-brand">Brand:${product.brand}</p><br>
                     <p class="product-category">Category:${product.category}</p><br>
                     <p class="product-price">Price:$${product.price}</p><br>
                </div>
                <form class="view-button" action="productDescription" method="post">
                    <input type="hidden" name="productId" value="${product.productId}">
                    <input  type="submit" value="View &raquo;" class="view-button" style="background-color: #04AA6D;color: white; ">
                </form>
            </div>
        </c:forEach>

    </div>
    <div class="pagination">

        <form  type ="hidden" class="previous" action="buyerHome.jsp" method="post">
            <input type="hidden" id="previousPage" name="page" value="${page-1}">
            <input type="hidden" name="offset" value="${firstProduct}">
            <input type="hidden" name="navOption" value="previous">
            <input type="submit" id="previous_button" value="previous &laquo;">
        </form>
        <br>
        <span id="pageNo" type="hidden">${page+1}</span>
        <input type="hidden" id="noOfProducts" value="${noOfProducts}">
        <br>
        <form  type="hidden" class="next" action="buyerHome" method="post">
            <input type="hidden" id="nextPage" name="page" value="${page+1}">
            <input type="hidden" name="navOption" value="next">
            <input type="hidden" name="offset" value="${lastProduct}">
            <input type="submit" id="next_button" value="next &raquo;">
        </form>

    </div>
    <div id="footer"></div>
    <script src="${pageContext.request.contextPath}/JS/buyerHome.js"></script>
</body>
</html>
