<%@page import="com.ecommerce.model.DbOperations.BeanInstance" %>
<%@ page import="com.ecommerce.model.DbOperations.ProductDAOImpl" %>
<%@ page import="com.ecommerce.model.product.Product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8"  %>
<script type = "text/javascript" >
  function preventBack()
  {
    window.history.forward();
  }
  setTimeout("preventBack()", 0);
  window.onunload=function(){null};
</script>
<html>
<head>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/buyer.css">
  <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
  <script>
    $(document).ready(function()
            {
              $("#header").load("header.html");
              $("#footer").load("footer.html");
            }
    );
  </script>
  <title>CART</title>
</head>
<body>
<div id="header"></div>
<div class="product-list">


  <c:forEach var="product" items="${cartMap}">

    <div class="product" id="p+${product.key}">
      <img class="product-image" src="data:image/jpeg;base64,${Product.displayImage(BeanInstance.getProductDAOType3().getImagePath(product.key))}" id="${product.key}" alt="productImage">
      <div class="product-button" >
        <button id="add-button${product.key}" class="add_button" onclick="sendAjaxRequest(${product.key},1)">+</button>
        <p id="count${product.key}" class="count">${product.value}</p>
        <p id="price${product.key}" class="price">$${productMap.get(product.key).price}</p>
        <button  id="remove-button${product.key}" class="remove-button"  onclick="sendAjaxRequest(${product.key},-1)">-</button>
        <form action="buy_now" method="post">
          <input type="hidden" name="productId" value="${product.key}">
          <input type="submit" id="buyNow${product.key}" class="buy-now"  value="Buy Now">
        </form>
      </div>
      <form class="view-button" action="productDescription" method="post">
        <input type="hidden" name="productId" value="${product.key}">
        <input  type="submit" value="View &raquo;" class="view-button" style="background-color: #04AA6D;color: white; ">
      </form>
    </div>
  </c:forEach>
  <div class="checkout-area">
    <label for="cartPrice">
      CartPrice:
    </label>
    <p id="cartPrice" >${BeanInstance.getCartDAOType3().getCartPrice(user.getUserId())}</p>
    <form method="post" action="checkout">
      <input type="submit" id="checkoutButton"  value="checkout">
    </form>

  </div>
</div>
<div id="footer"></div>
<script src="${pageContext.request.contextPath}/JS/cart.js"></script>
</body>
</html>
