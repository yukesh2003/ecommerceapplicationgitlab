<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.ecommerce.model.product.Order" %>
<%@ page import="com.ecommerce.model.DbOperations.BeanInstance" %>
<%@ page import="com.ecommerce.model.DbOperations.ProductDAOImpl" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.ecommerce.model.product.Product" %>
<%@ page import="com.ecommerce.model.user.User" %>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<script type = "text/javascript" >
    function preventBack()
    {
        window.history.forward();
    }
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
</script>
<%
    try
    {
       Set<Order> orderList=  BeanInstance.getOrderDAOType3().getOrders(((User)request.getSession().getAttribute("user")).getUserId());
       Map<Integer, Product> productMap= BeanInstance.getProductDAOType3().getProductMap();
       request.setAttribute("productMap",productMap);
       request.setAttribute("orderList",orderList);
    }
    catch (SQLException e)
    {
        throw new RuntimeException(e);
    }

%>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/order.css">
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function()
            {
                $("#header").load("header.html");
                $("#footer").load("footer.html");
            }
        );
        <% request.setAttribute("user",request.getSession().getAttribute("user"));%>
    </script>
    <title>Orders</title>
</head>
<body>
<div id="header"></div>
    <div class="order-list">
        <c:forEach var="order" items="${orderList}">

            <div class="order" id="order${order.orderId}">
                <div class="order-info">
            <p class="orderId" id="orderId${order.orderId}" >Order Id:${order.orderId}</p>
            <p class="orderedDate" id="orderedDate${order.orderId}">Ordered Date:${order.orderedDate}</p>
            <p class="orderStatus" id="orderStatus${order.orderId}">Order Status:${order.orderStatus}</p>
            <p class="deliveredDate" id="deliveredDate${order.orderId}">Delivered date:${order.deliveredDate}</p>
                </div>
            <c:forEach var="mapEntry" items="${order.itemsCount}">
                <div class="orderProduct">
                    <img class="product-image" src="data:image/jpeg;base64,${Product.displayImage(BeanInstance.getProductDAOType3().getImagePath(mapEntry.key))}" id="image${mapEntry.key}" alt="productImage">
                    <div class="productText">
                    <p class="productName">Product Name:${productMap.get(mapEntry.key).name}</p>
                    <p class="productCount">Product Count:${mapEntry.value}</p>
                    <p class="productPrice">Product Price:${productMap.get(mapEntry.key).price}</p>
                    </div>
                </div>
            </c:forEach>
            </div>
        </c:forEach>
    </div>
<div id="footer"></div>
</body>
</html>
