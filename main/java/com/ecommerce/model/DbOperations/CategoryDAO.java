package com.ecommerce.model.DbOperations;

import com.ecommerce.model.product.Category;

import java.sql.SQLException;
import java.util.Set;

public interface CategoryDAO{

    Integer getCategoryId(String categoryName) throws SQLException;
    String getCategoryName(int category) throws SQLException;
    boolean addCategory(Category category) throws SQLException;
    Set<Category> getCategories() throws SQLException;
}
