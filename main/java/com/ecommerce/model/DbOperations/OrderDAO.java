package com.ecommerce.model.DbOperations;
import com.ecommerce.model.product.Order;
import java.sql.SQLException;
import java.util.Set;

public interface OrderDAO {
    Set<Order> getOrders(int userId) throws SQLException;
    Order getOrder(int orderId) throws SQLException;
    void addOrder(Order order) throws SQLException;
    void changeOrderStatus(int orderId) throws SQLException;

}
