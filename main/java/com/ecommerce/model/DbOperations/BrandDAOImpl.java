package com.ecommerce.model.DbOperations;
import com.adventnet.db.api.RelationalAPI;
import com.adventnet.ds.query.*;
import com.adventnet.persistence.DataAccess;
import com.adventnet.persistence.DataAccessException;
import com.adventnet.persistence.Row;
import com.ecommerce.model.product.Brand;
import java.sql.*;
import java.util.LinkedHashSet;
import java.util.Set;

public class BrandDAOImpl implements BrandDAO{

//    public static BrandDAO getInstance() {
//        try {
//            return (BrandDAO) BeanUtil.lookup("aaa");
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//    }

    public  Integer getBrandId(String brandName) throws SQLException {
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Brand"));
        selectQuery.addSelectColumn(Column.getColumn("Brand","BRAND_ID"));
        selectQuery.setCriteria(new Criteria(Column.getColumn("Brand","BRAND_NAME"),brandName,QueryConstants.EQUAL));
        try(DataSet dataSet= RelationalAPI.getInstance().executeQuery(selectQuery,DbUtil.getConnection()))
        {
            if (dataSet.next())
            {
                return (Integer) dataSet.getValue("BRAND_ID");
            }
        } catch (QueryConstructionException e) {
            throw new RuntimeException(e);
        }

    return null;
    }

    public  String getBrandName(int brandId) throws SQLException
    {
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Brand"));
        selectQuery.addSelectColumn(Column.getColumn("Brand","BRAND_NAME"));
        selectQuery.setCriteria(new Criteria(Column.getColumn("Brand","BRAND_ID"),brandId,QueryConstants.EQUAL));
        try(DataSet dataSet= RelationalAPI.getInstance().executeQuery(selectQuery,DbUtil.getConnection()))
        {
            if (dataSet.next())
            {
                return (String) dataSet.getValue("BRAND_NAME");
            }
        } catch (QueryConstructionException e) {
            throw new RuntimeException(e);
        }

        return null;
    }
    public boolean addBrand(Brand brand) throws SQLException {
        InsertQueryImpl insertQuery = new InsertQueryImpl("Brand",1);
        Row row = new Row("Brand");
        row.set("BRAND_NAME",brand.getName());
        insertQuery.addRow(row);
        try
        {
            DataAccess.add(insertQuery);
            return true;
        }
        catch (DataAccessException e)
        {
            return false;
        }
    }

    public Set<Brand> getBrands() throws SQLException
    {
        Set<Brand> brandSet = new LinkedHashSet<>();
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Brand"));
        selectQuery.addSelectColumn(Column.getColumn("Brand","BRAND_ID"));
        selectQuery.addSelectColumn(Column.getColumn("Brand","BRAND_NAME"));
        selectQuery.addSortColumn(new SortColumn("Brand","BRAND_ID",true));
        RelationalAPI relationalAPI = RelationalAPI.getInstance();
        try(DataSet dataSet= relationalAPI.executeQuery(selectQuery,DbUtil.getConnection()))
        {
            while (dataSet.next())
                    {
                        Brand brand = new Brand(dataSet.getAsString("BRAND_NAME"));
                        brand.setBrandId((Integer) dataSet.getValue("BRAND_ID"));
                        brandSet.add(brand);
                    }
        }
        catch (QueryConstructionException e)
        {
            return null;
        }
        return brandSet;
    }

    }


