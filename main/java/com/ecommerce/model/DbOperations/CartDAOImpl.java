package com.ecommerce.model.DbOperations;
import com.adventnet.db.api.RelationalAPI;
import com.adventnet.ds.query.*;
import com.adventnet.mfw.bean.BeanUtil;
import com.adventnet.persistence.*;
import com.ecommerce.model.product.Cart;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CartDAOImpl implements CartDAO{
    public  Integer getCartId(int userId) throws SQLException {
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Cart"));
        selectQuery.addSelectColumn(Column.getColumn("Cart","CART_ID"));
        selectQuery.setCriteria(new Criteria(Column.getColumn("Cart","BUYER_ID"),userId,QueryConstants.EQUAL));
        try(DataSet dataSet= RelationalAPI.getInstance().executeQuery(selectQuery,DbUtil.getConnection()))
        {
            if (dataSet.next())
            {
                return (Integer) dataSet.getValue("CART_ID");
            }
        }
        catch (QueryConstructionException e)
        {
            throw new RuntimeException(e);
        }
        return null;
    }
    private int getProductCount(int cartId,int productId) throws SQLException {
        int count=0;
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("CartItems"));
        selectQuery.addSelectColumn(Column.getColumn("CartItems","COUNT"));
        selectQuery.setCriteria(new Criteria(Column.getColumn("CartItems","CART_ID"),cartId,QueryConstants.EQUAL).and(new Criteria(new Column("CartItems","PRODUCT_ID"),productId,QueryConstants.EQUAL)));
        try(DataSet dataSet= RelationalAPI.getInstance().executeQuery(selectQuery,DbUtil.getConnection()))
        {
            if (dataSet.next())
            {
                return (Integer) dataSet.getValue("COUNT");
            }
        }
        catch (QueryConstructionException e)
        {
            throw new RuntimeException(e);
        }
      return 0;
    }
    public  Integer getCartPrice(Integer userId) throws SQLException {
        Cart cart=getCart(userId);
        int cartPrice=0;
        Map<Integer,Integer> cartItems=cart.getItems();
        Set<Integer> keyset=cartItems.keySet();
        for(Integer key:keyset)
        {
            cartPrice+=(cartItems.get(key)* BeanInstance.getProductDAOType3().getPrice(key));
        }
        return cartPrice;
    }
    public  Cart getCart(Integer buyerId) throws SQLException {
        Cart cart= new Cart();
        Map<Integer,Integer> items= new HashMap<>();
        int cartId = getCartId(buyerId);
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("CartItems"));
        selectQuery.addSelectColumn(Column.getColumn("CartItems","PRODUCT_ID"));
        selectQuery.addSelectColumn(Column.getColumn("CartItems","COUNT"));
        selectQuery.setCriteria(new Criteria(Column.getColumn("CartItems","CART_ID"),cartId,QueryConstants.EQUAL));
        RelationalAPI relationalAPI = RelationalAPI.getInstance();
               try(DataSet dataSet = relationalAPI.executeQuery(selectQuery,DbUtil.getConnection()))
               {
                   while (dataSet.next())
                   {
                       int productId = (int) dataSet.getValue(1);
                       int count = (int) dataSet.getValue(2);
                       items.put(productId, count);
                   }
               }
               catch (QueryConstructionException e)
               {
                   throw new RuntimeException(e);
               }
            cart.setItems(items);
            cart.setCartId(cartId);
            return cart;
        }

    public void removeCartEntry(int cartId, int productId) throws SQLException {
       DeleteQuery deleteQuery = new DeleteQueryImpl("CartItems");
       deleteQuery.setCriteria(new Criteria(Column.getColumn("CartItems","CART_ID"),cartId,QueryConstants.EQUAL) .and(new Criteria(Column.getColumn("CartItems","PRODUCT_ID"),productId,QueryConstants.EQUAL)));
        try
        {
            Persistence persistence = (Persistence) BeanUtil.lookup("Persistence");
            persistence.delete(deleteQuery);
        } catch (Exception e)
        {
            throw new RuntimeException(e);
        }

    }
    public int addToCart(int userId, int productId, int count) throws SQLException
    {
        try  {
            Integer cartId = getCartId(userId);
            Logger.getAnonymousLogger().log(Level.SEVERE, String.valueOf(cartId)+"-----------");
            if (cartId == null)
            {
                Logger.getAnonymousLogger().log(Level.SEVERE, "----In cartId == null-------");
                DataObject dataObject = new WritableDataObject();
                Row row = new Row("Cart");
                row.set("BUYER_ID",userId);
                dataObject.addRow(row);
                DataAccess.add(dataObject);
                cartId = row.getInt("CART_ID");
                dataObject.deleteRow(row);
                Row row1 = new Row("CartItems");
                row1.set("CART_ID",cartId);
                row1.set("PRODUCT_ID",productId);
                row1.set("COUNT",count);
                dataObject.addRow(row1);
                DataAccess.add(dataObject);
                Logger.getAnonymousLogger().log(Level.SEVERE, "----In Cart Items added state-------");
            }
            else
            {
               int dbCount= getProductCount(cartId,productId);
               if(dbCount==0)
               {
                   Logger.getAnonymousLogger().log(Level.SEVERE, "----In dbcount == o 128-------");
                   DataObject dataObject = new WritableDataObject();
                   Row row = new Row("CartItems");
                   row.set("CART_ID",cartId);
                   row.set("PRODUCT_ID",productId);
                   row.set("COUNT",count);
                   dataObject.addRow(row);
                   DataAccess.update(dataObject);
               }
               else
               {
//                   DataObject dataObject = new WritableDataObject();
//                   Row row = new Row("CartItems");
//                   row.set("CART_ID",cartId);
//                   row.set("PRODUCT_ID",productId);
//                   row.set("COUNT",count);
//                   dataObject.addRow(row);
//                   DataAccess.update(dataObject);
                  UpdateQuery updateQuery = new UpdateQueryImpl("CartItems");
                  updateQuery.setUpdateColumn("COUNT",count+getProductCount(cartId,productId));
                  updateQuery.setCriteria(new Criteria(Column.getColumn("CartItems","CART_ID"),cartId,QueryConstants.EQUAL) .and(new Criteria(new Column("CartItems","PRODUCT_ID"),productId,QueryConstants.EQUAL)));
                  DataAccess.update(updateQuery);
               }
            }
            int dbCount= getProductCount(cartId,productId);
            if(dbCount==0)
            {
                removeCartEntry(cartId,productId);
            }
            return dbCount;
        }
        catch (DataAccessException e)
        {
            throw new RuntimeException(e);
        }
    }
}
