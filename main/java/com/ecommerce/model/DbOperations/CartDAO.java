package com.ecommerce.model.DbOperations;

import com.ecommerce.model.product.Cart;
import java.sql.Connection;
import java.sql.SQLException;

public interface CartDAO {
    Integer getCartId(int userId) throws SQLException;
    Integer getCartPrice(Integer userId) throws SQLException;
    Cart getCart(Integer userId) throws SQLException;
    void removeCartEntry(int cartId, int productId) throws SQLException;//todo
    int addToCart(int userId, int productId, int count) throws SQLException;

}
