package com.ecommerce.model.DbOperations;
import com.adventnet.db.api.RelationalAPI;
import java.sql.*;
public class DbUtil
{
    public static Connection getConnection() throws SQLException
    {
            return RelationalAPI.getInstance().getConnection();
    }


}
