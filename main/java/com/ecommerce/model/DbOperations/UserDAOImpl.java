package com.ecommerce.model.DbOperations;
import com.adventnet.db.api.RelationalAPI;
import com.adventnet.ds.query.*;
import com.adventnet.persistence.*;
import com.ecommerce.model.user.*;
import com.ecommerce.model.util.Enums;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserDAOImpl implements UserDAO{

    @Override
    public  Address getUserAddress(int userId)
    {
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("UserAddress"));
        Criteria criteria = new Criteria(Column.getColumn("UserAddress", "USER_ID"), userId, 0);
        selectQuery.setCriteria(criteria);
        List<Column> selectColumns = new ArrayList<>();
        selectColumns.add(Column.getColumn("UserAddress", "LOCALITY"));
        selectColumns.add(Column.getColumn("UserAddress", "DISTRICT"));
        selectColumns.add(Column.getColumn("UserAddress", "STATE"));
        selectColumns.add(Column.getColumn("UserAddress", "PIN"));
        selectQuery.addSelectColumns(selectColumns);
        try {
            DataObject dataObject = DataAccess.get(selectQuery);
            if(dataObject.getRows("UserAddress").hasNext())
            {
                Row row = dataObject.getRow("UserAddress");
                Address address = new Address();
                address.setLocality(row.getString("LOCALITY"));
                address.setDistrict(row.getString("DISTRICT"));
                address.setState(row.getString("STATE"));
                address.setPinCode(row.getInt("PIN"));
                return address;
            }
        }
        catch (DataAccessException e)
        {
            throw new RuntimeException(e);
        }
        return null;
    }

    public  User isAuthenticatedUser(String userName, String password) throws SQLException {
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("User"));
        selectQuery.addSelectColumn(Column.getColumn("User","USER_ID"));
        selectQuery.addSelectColumn(Column.getColumn("User","USER_TYPE"));
        Join join = new Join("User","UserAuthentication",new String[] {"USER_ID"},new String[]{"USER_ID"},Join.INNER_JOIN);
        selectQuery.addJoin(join);
        Criteria criteria = new Criteria(new Column("User","EMAIL"),userName,QueryConstants.EQUAL);
        Criteria criteria1=criteria .and (new Criteria(new Column("UserAuthentication","PASSWORD"),password,QueryConstants.EQUAL));
        selectQuery.setCriteria(criteria1);
        try
        {
            Logger.getAnonymousLogger().log(Level.SEVERE,RelationalAPI.getInstance().getSelectSQL(selectQuery));;
        }
        catch (QueryConstructionException e)
        {
            throw new RuntimeException(e);
        }
        Connection connection = DbUtil.getConnection();
        User user= null;
        RelationalAPI relationalAPI = RelationalAPI.getInstance();
        try (  DataSet dataSet1 = relationalAPI.executeQuery(selectQuery,connection);)
        {
            while (dataSet1.next())
            {
                int userType = (int) dataSet1.getValue("USER_TYPE");
                if(userType==0)
                {
                    user= new Buyer(userName);
                    user.setUserId((Integer) dataSet1.getValue("USER_ID"));
                }
                else if(userType==1)
                {
                    user= new Seller(userName);
                    user.setUserId((Integer) dataSet1.getValue("USER_ID"));
                }
                else if(userType==2)
                {
                    user= new DeliveryAgent(userName);
                    user.setUserId((Integer) dataSet1.getValue("USER_ID"));
                }
                else if(userType==3)
                {
                    user= new Administrator(userName);
                    user.setUserId((Integer) dataSet1.getValue("USER_ID"));
                }
            }

        }
        catch (QueryConstructionException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connection.close();
        }
        return user;
    }
    public  boolean addUser(User user) throws DataAccessException {
        DataObject dataObject = new WritableDataObject();
        Row row = new Row("User");
        row.set("PHONE",user.getPhone());
        row.set("EMAIL",user.getEmail());
        row.set("NAME",user.getName());
        row.set("STATUS",0);
        row.set("USER_TYPE",user.getUserType().ordinal());
        Integer userId;
        dataObject.addRow(row);
        DataAccess.add(dataObject);
        userId=row.getInt("USER_ID");
/*        Logger.getAnonymousLogger().log(Level.SEVERE,"UserId is:"+userId);*/
        if(userId!=null)
        {
            Row row5 = new Row("UserAuthentication");
            row5.set("USER_ID",userId);
            row5.set("PASSWORD",user.getLogin().getPassword());
            dataObject.addRow(row5);
            Row row2 = new Row("UserAddress");
            Address address=user.getAddress();
            row2.set("USER_ID",userId);
            row2.set("DISTRICT",address.getDistrict());
            row2.set("LOCALITY",address.getLocality());
            row2.set("PIN",address.getPinCode());
            row2.set("STATE",address.getState());
            dataObject.addRow(row2);
            Row row3;
            if(user.getUserType().equals(Enums.UserType.BUYER))
            {
                row3 = new Row("Buyer");
                row3.set("USER_ID",userId);
                row3.set("GENDER",((Buyer)user).getGender().ordinal());
                dataObject.addRow(row3);
            }
            else if (user.getUserType().equals(Enums.UserType.SELLER) )
            {
                row3 = new Row("Vendor");
                row3.set("USER_ID",userId);
                dataObject.addRow(row3);
            }
            else if (user.getUserType().equals(Enums.UserType.DELIVERY_AGENT) )
            {
                row3 = new Row("DeliveryAgent");
                row3.set("USER_ID",userId);
                dataObject.addRow(row3);
            }
            else if (user.getUserType().equals(Enums.UserType.ADMINISTRATOR) )
            {
                row3 = new Row("Administrator");
                row3.set("USER_ID",userId);
                dataObject.addRow(row3);
            }
            DataAccess.update(dataObject);
            return true;
            }
        return false;
    }


    public  boolean registerBank(BankAccount bankAccount){
        Row row = new Row("UserBankInfo");
        row.set("USER_ID",bankAccount.getUserId());
        row.set("ACCOUNT_NO",bankAccount.getAccountNo());
        row.set("BANK_NAME",bankAccount.getBank());
        row.set("IFSC",bankAccount.getIfsc());
        DataObject dataObject= new WritableDataObject();
        try
        {
            dataObject.addRow(row);
            DataAccess.add(dataObject);
            return true;
        }
        catch (DataAccessException e)
        {
            throw new RuntimeException("Cannot Update Bank Account ");
        }
    }

    public Set<Seller> getSellers() throws SQLException{
//    {
//        Set<Seller> sellerSet = new HashSet<>();
//        DataObject dataObject = new WritableDataObject();
//        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("User"));
//        List<Column> field = new ArrayList<>();
//        field.add(Column.getColumn("User", "USER_ID"));
//        field.add(Column.getColumn("User", "PHONE"));
//        field.add(Column.getColumn("User", "EMAIL"));
//        field.add(Column.getColumn("User", "NAME"));
//        field.add(Column.getColumn("User", "STATUS"));
//        field.add(Column.getColumn("UserAddress", "LOCALITY"));
//        field.add(Column.getColumn("UserAddress", "DISTRICT"));
//        field.add(Column.getColumn("UserAddress", "STATE"));
//        field.add(Column.getColumn("UserAddress", "PIN"));
//        Join join = new Join("User", "UserAddress", new String[]{"USER_ID"}, new String[]{"USER_ID"}, Join.INNER_JOIN);
//        selectQuery.addSelectColumns(field);
//        selectQuery.addJoin(join);
//        selectQuery.setCriteria(new Criteria(new Column("User", "USER_TYPE"), 1, QueryConstants.EQUAL));
//        dataObject.
        Set<Seller> sellerSet = new HashSet<>();
        try (Connection connection = DbUtil.getConnection()) {
            SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("User"));
            List<Column> field = new ArrayList<>();
            field.add(Column.getColumn("User", "USER_ID"));
            field.add(Column.getColumn("User", "PHONE"));
            field.add(Column.getColumn("User", "EMAIL"));
            field.add(Column.getColumn("User", "NAME"));
            field.add(Column.getColumn("User", "STATUS"));
            field.add(Column.getColumn("UserAddress", "LOCALITY"));
            field.add(Column.getColumn("UserAddress", "DISTRICT"));
            field.add(Column.getColumn("UserAddress", "STATE"));
            field.add(Column.getColumn("UserAddress", "PIN"));
            Join join = new Join("User", "UserAddress", new String[]{"USER_ID"}, new String[]{"USER_ID"}, Join.INNER_JOIN);
            selectQuery.addSelectColumns(field);
            selectQuery.addJoin(join);
            selectQuery.setCriteria(new Criteria(new Column("User", "USER_TYPE"), 1, QueryConstants.EQUAL));
            RelationalAPI relationalAPI = RelationalAPI.getInstance();
            try (DataSet dataSet = relationalAPI.executeQuery(selectQuery, connection)) {
                while (dataSet.next()) {
                    Seller seller = new Seller(dataSet.getAsString("EMAIL"));
                    seller.setPhone(dataSet.getAsString("PHONE"));
                    seller.setName(dataSet.getAsString("NAME"));
                    Address address = new Address();
                    address.setDistrict(dataSet.getAsString("DISTRICT"));
                    address.setLocality(dataSet.getAsString("LOCAlITY"));
                    address.setState(dataSet.getAsString("STATE"));
                    address.setPinCode((Integer) dataSet.getValue("PIN"));
                    seller.setAddress(address);
                    seller.setStatus(((Integer) dataSet.getValue("STATUS")));
                    seller.setUserId((Integer) dataSet.getValue("USER_ID"));
                    sellerSet.add(seller);
                }
            } catch (QueryConstructionException e) {
                throw new RuntimeException(e);
            }
            return sellerSet;
        }
    }

    public  Set<DeliveryAgent> getDeliveryAgents() throws SQLException {
        Set<DeliveryAgent> deliveryAgentSet = new HashSet<>();
        try (Connection connection = DbUtil.getConnection()) {
            SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("User"));
            List<Column> field = new ArrayList<>();
            field.add(Column.getColumn("User", "USER_ID"));
            field.add(Column.getColumn("User", "PHONE"));
            field.add(Column.getColumn("User", "EMAIL"));
            field.add(Column.getColumn("User", "NAME"));
            field.add(Column.getColumn("User","STATUS"));
            field.add(Column.getColumn("UserAddress", "LOCALITY"));
            field.add(Column.getColumn("UserAddress", "DISTRICT"));
            field.add(Column.getColumn("UserAddress", "STATE"));
            field.add(Column.getColumn("UserAddress", "PIN"));
            Join join = new Join("User", "UserAddress", new String[]{"USER_ID"}, new String[]{"USER_ID"}, Join.INNER_JOIN);
            selectQuery.addSelectColumns(field);
            selectQuery.addJoin(join);
            selectQuery.setCriteria(new Criteria(new Column("User", "USER_TYPE"), 2, QueryConstants.EQUAL));
            RelationalAPI relationalAPI = RelationalAPI.getInstance();
            try (DataSet dataSet = relationalAPI.executeQuery(selectQuery, connection)) {
                while (dataSet.next()) {
                    DeliveryAgent deliveryAgent = new DeliveryAgent(dataSet.getAsString("EMAIL"));
                    deliveryAgent.setPhone(dataSet.getAsString("PHONE"));
                    deliveryAgent.setName(dataSet.getAsString("NAME"));
                    deliveryAgent.setStatus(((Integer) dataSet.getValue("STATUS")));
                    Address address = new Address();
                    address.setDistrict(dataSet.getAsString("DISTRICT"));
                    address.setLocality(dataSet.getAsString("LOCAlITY"));
                    address.setState(dataSet.getAsString("STATE"));
                    address.setPinCode((Integer) dataSet.getValue("PIN"));
                    deliveryAgent.setAddress(address);
                    deliveryAgent.setUserId((Integer) dataSet.getValue("USER_ID"));
                    deliveryAgentSet.add(deliveryAgent);
                }
            }
            catch (QueryConstructionException e) {
                throw new RuntimeException(e);
            }
            return deliveryAgentSet;
        }
    }
    public  boolean removeUser(int userId)
    {
        try
        {
            DataObject dataObject =DataAccess.get("User",new Criteria(Column.getColumn("User","USER_ID"),userId,QueryConstants.EQUAL));
            Row row=dataObject.getRow("User");
            Logger.getAnonymousLogger().log(Level.SEVERE,row.getString("STATUS")+"--------------------------");
            row.set("STATUS",1);
            dataObject.updateRow(row);
            DataAccess.update(dataObject);
            return true;
        }
        catch (DataAccessException e)
        {
            throw new RuntimeException(e);
        }
    }
}


