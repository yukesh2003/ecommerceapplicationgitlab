package com.ecommerce.model.DbOperations;
import com.adventnet.db.api.RelationalAPI;
import com.adventnet.ds.query.*;
import com.adventnet.persistence.*;
import com.ecommerce.model.product.Product;
import com.ecommerce.model.product.ProductDescription;
import com.ecommerce.model.util.Enums;
import com.ecommerce.model.util.FilterCriteria;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.ecommerce.model.util.FilterCriteria.FilterType.BRAND;

public class ProductDAOImpl implements ProductDAO{
//    public  Integer getProductId(int sellerId, String name,Connection connection) throws SQLException {
//
//            try(PreparedStatement statement = connection.prepareStatement("select product_id from product where ( seller_Id=? and name=? )"))
//            {
//                statement.setInt(1, sellerId);
//                statement.setString(2, name);
//                try (ResultSet resultSet = statement.executeQuery())
//                {
//                    if(resultSet.next())
//                    {
//                        return resultSet.getInt(1);
//                    }
//                }
//                return null;
//            }
//    }
   public  Double getPrice(int productId) throws SQLException  {
        try(Connection connection= DbUtil.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Product"));
            selectQuery.addSelectColumn(Column.getColumn("Product","PRICE"));
            selectQuery.setCriteria(new Criteria(new Column("Product","PRODUCT_ID"),productId,QueryConstants.EQUAL));
                try (DataSet resultSet = RelationalAPI.getInstance().executeQuery(selectQuery,connection))
                {
                   if (resultSet.next())
                   {
                        return (Double) resultSet.getValue(1);
                   }
                } catch (QueryConstructionException e) {
                    throw new RuntimeException(e);
                }
        }
       return null;
   }

    public  boolean addProduct(Product product) throws SQLException {

            try
            {
                Row row = new Row("Product");
                row.set("SELLER_ID", (product.getSellerId()));
                row.set("BRAND_ID", BeanInstance.getBrandDAOType1().getBrandId(product.getBrand()));
                row.set("PRICE", product.getPrice());
                row.set("CATEGORY_ID", BeanInstance.getCategoryDAOType1().getCategoryId(product.getCategory()));
                row.set("NAME", product.getName());
                row.set("IMAGE", product.getImagePath());
                InsertQueryImpl insertQuery = new InsertQueryImpl("Product",1);
                insertQuery.addRow(row);
                DataAccess.add(insertQuery);
                Row row1 = new Row("ProductDescription");
                row1.set("PRODUCT_ID",row.get("PRODUCT_ID"));
                row1.set("COLOR", product.getDescription().getColor());
                row1.set("SIZE", product.getDescription().getSize());
                row1.set("IDEAL_FOR", product.getDescription().getCustomerGroup());
                InsertQueryImpl insertQuery1= new InsertQueryImpl("ProductDescription",1);
                insertQuery1.addRow(row1);
                DataAccess.add(insertQuery1);
            }
            catch (DataAccessException e)
            {
                Logger.getAnonymousLogger().log(Level.SEVERE,e.getLocalizedMessage());
                return false;
            }
            return true;

    }

    public Set<Product> getProducts() throws SQLException {
        Set<Product> productSet = new LinkedHashSet<>();
        try (Connection connection = DbUtil.getConnection()) {
            SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Product"));
            selectQuery.addSelectColumn(new Column("Product", "NAME"));
            selectQuery.addSelectColumn(new Column("Product", "PRICE"));
            selectQuery.addSelectColumn(new Column("Product", "BRAND_ID"));
            selectQuery.addSelectColumn(new Column("Product", "CATEGORY_ID"));
            selectQuery.addSelectColumn(new Column("Product", "PRODUCT_ID"));
            selectQuery.addSelectColumn(new Column("Product", "IMAGE"));
            try (DataSet resultSet = RelationalAPI.getInstance().executeQuery(selectQuery, connection))
            {
                while (resultSet.next())
                {
                    Product product = new Product();
                    product.setProductId((Integer) resultSet.getValue(5));
                    product.setName(resultSet.getAsString(1));
                    product.setPrice((Double) resultSet.getValue(2));
                    product.setBrand(BeanInstance.getBrandDAOType1().getBrandName((Integer) resultSet.getValue(3)));
                    product.setCategory(BeanInstance.getCategoryDAOType1().getCategoryName((Integer) resultSet.getValue(4)));
                    product.setImagePath(resultSet.getAsString(6));
                    Logger.getAnonymousLogger().log(Level.SEVERE,product.getBrand()+product.getName()+"--------------=======-------------------------");
                    productSet.add(product);
                }
            }
            catch (QueryConstructionException e)
            {
                throw new RuntimeException(e);
            }
            return productSet;
        }
    }
    public Set<Product> getProducts(int sellerId) throws SQLException {
       Set<Product> productSet = new LinkedHashSet<>();
        try (Connection connection = DbUtil.getConnection()) {
            SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Product"));
            selectQuery.addSelectColumn(new Column("Product", "NAME"));
            selectQuery.addSelectColumn(new Column("Product", "PRICE"));
            selectQuery.addSelectColumn(new Column("Product", "BRAND_ID"));
            selectQuery.addSelectColumn(new Column("Product", "CATEGORY_ID"));
            selectQuery.addSelectColumn(new Column("Product", "PRODUCT_ID"));
            selectQuery.addSelectColumn(new Column("Product", "IMAGE"));
            selectQuery.setCriteria(new Criteria(new Column("Product", "SELLER_ID"), sellerId, QueryConstants.EQUAL));
            try (DataSet resultSet = RelationalAPI.getInstance().executeQuery(selectQuery, connection)) {
                while (resultSet.next()) {
                    Product product = new Product();
                    product.setProductId((Integer) resultSet.getValue(5));
                    product.setName(resultSet.getAsString(1));
                    product.setPrice((Double) resultSet.getValue(2));
                    product.setBrand(BeanInstance.getBrandDAOType1().getBrandName((Integer) resultSet.getValue(3)));
                    product.setCategory(BeanInstance.getCategoryDAOType1().getCategoryName((Integer) resultSet.getValue(4)));
                    product.setImagePath(resultSet.getAsString(6));
                    Logger.getAnonymousLogger().log(Level.SEVERE,product.getBrand()+product.getName()+"--------------=======-------------------------");
                    productSet.add(product);
                }
            }
            catch (QueryConstructionException e)
            {
                throw new RuntimeException(e);
            }
            return productSet;
        }
    }

    public Set<Product> getProducts(FilterCriteria filterCriteria, int productId, Enums.Navigation navigation) throws SQLException {
        try {
            String text = " ";
            FilterCriteria.FilterType option = FilterCriteria.FilterType.NAME;
            if (filterCriteria.getValue() != null) {
                text = filterCriteria.getValue();
            }
            if (filterCriteria.getType() != null) {
                option = filterCriteria.getType();
            }
            Set<Product> productSet = new LinkedHashSet<>();
            switch (option) {

                case NAME: {
                    if (navigation.equals(Enums.Navigation.PREVIOUS)) {
//                        statement = connection.prepareStatement("select * from(SELECT name, price, brand_id, category_id, product_id, image, seller_id from Product" +
//                                " where NAME like ? and   PRODUCT_ID < ?  order by PRODUCT_ID desc limit 5)  as `P*` order by PRODUCT_ID");
                        try (Connection connection = DbUtil.getConnection()) {
                            SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Product"));
                            selectQuery.addSelectColumn(new Column("Product", "NAME"));
                            selectQuery.addSelectColumn(new Column("Product", "PRICE"));
                            selectQuery.addSelectColumn(new Column("Product", "BRAND_ID"));
                            selectQuery.addSelectColumn(new Column("Product", "CATEGORY_ID"));
                            selectQuery.addSelectColumn(new Column("Product", "PRODUCT_ID"));
                            selectQuery.addSelectColumn(new Column("Product", "IMAGE"));
                            selectQuery.addSelectColumn(new Column("Product", "SELLER_ID"));
                            Criteria criteria = new Criteria(new Column("Product", "NAME"), text, QueryConstants.LIKE);
                            Criteria criteria1 = criteria.and(new Criteria(new Column("Product", "PRODUCT_ID"), productId, QueryConstants.LESS_THAN));
                            selectQuery.setCriteria(criteria1);
                            selectQuery.addSortColumn(new SortColumn("Product", "PRODUCT_ID", false));
                            selectQuery.setRange(new Range(0, 5));
                            try (DataSet resultSet = RelationalAPI.getInstance().executeQuery(selectQuery, connection)) {
                                while (resultSet.next()) {
                                    Product product = new Product();
                                    product.setProductId((Integer) resultSet.getValue(5));
                                    product.setName(resultSet.getAsString(1));
                                    product.setPrice((Double) resultSet.getValue(2));
                                    product.setBrand(BeanInstance.getBrandDAOType1().getBrandName((Integer) resultSet.getValue(3)));
                                    product.setCategory(BeanInstance.getCategoryDAOType1().getCategoryName((Integer) resultSet.getValue(4)));
                                    product.setImagePath(resultSet.getAsString(6));
                                    productSet.add(product);
                                }
                            } catch (QueryConstructionException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    } else {
//                        statement = connection.prepareStatement("SELECT name, price, brand_id, category_id, product_id, image, seller_id from Product where NAME like ? and PRODUCT_ID > ? order by PRODUCT_ID limit 5");
                        try (Connection connection = DbUtil.getConnection()) {
                            SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Product"));
                            selectQuery.addSelectColumn(new Column("Product", "NAME"));
                            selectQuery.addSelectColumn(new Column("Product", "PRICE"));
                            selectQuery.addSelectColumn(new Column("Product", "BRAND_ID"));
                            selectQuery.addSelectColumn(new Column("Product", "CATEGORY_ID"));
                            selectQuery.addSelectColumn(new Column("Product", "PRODUCT_ID"));
                            selectQuery.addSelectColumn(new Column("Product", "IMAGE"));
                            selectQuery.addSelectColumn(new Column("Product", "SELLER_ID"));
                            Criteria criteria = new Criteria(new Column("Product", "NAME"), text, QueryConstants.LIKE);
                            Criteria criteria1 = criteria.and(new Criteria(new Column("Product", "PRODUCT_ID"), productId, QueryConstants.LESS_THAN));
                            selectQuery.setCriteria(criteria1);
                            selectQuery.addSortColumn(new SortColumn("Product", "PRODUCT_ID", true));
                            selectQuery.setRange(new Range(0, 5));
                            try (DataSet resultSet = RelationalAPI.getInstance().executeQuery(selectQuery, connection)) {
                                while (resultSet.next()) {
                                    Product product = new Product();
                                    product.setProductId((Integer) resultSet.getValue(5));
                                    product.setName(resultSet.getAsString(1));
                                    product.setPrice((Double) resultSet.getValue(2));
                                    product.setBrand(BeanInstance.getBrandDAOType1().getBrandName((Integer) resultSet.getValue(3)));
                                    product.setCategory(BeanInstance.getCategoryDAOType1().getCategoryName((Integer) resultSet.getValue(4)));
                                    product.setImagePath(resultSet.getAsString(6));
                                    productSet.add(product);
                                }
                            } catch (QueryConstructionException e) {
                                throw new RuntimeException(e);
                            }

                        }
                    }
                    break;
                }

                case BRAND: {
                    if (navigation.equals(Enums.Navigation.PREVIOUS)) {
                        try (Connection connection = DbUtil.getConnection()) {
//                        statement = connection.prepareStatement("select * from(SELECT  Product.name,price,B.brand_id,category_id,product_id,image,seller_id from  product inner join Brand B on Product.BRAND_ID = B.BRAND_ID where B.NAME like ? and PRODUCT_ID < ?   order by PRODUCT_ID desc limit 5)  as `pB*` order by PRODUCT_ID");
                            SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Product"));
                            selectQuery.addSelectColumn(new Column("Product", "NAME"));
                            selectQuery.addSelectColumn(new Column("Product", "PRICE"));
                            selectQuery.addSelectColumn(new Column("Product", "BRAND_ID"));
                            selectQuery.addSelectColumn(new Column("Product", "CATEGORY_ID"));
                            selectQuery.addSelectColumn(new Column("Product", "PRODUCT_ID"));
                            selectQuery.addSelectColumn(new Column("Product", "IMAGE"));
                            selectQuery.addSelectColumn(new Column("Product", "SELLER_ID"));
                            selectQuery.addJoin(new Join("Product", "Brand", new String[]{"BRAND_ID"}, new String[]{"BRAND_ID"}, Join.INNER_JOIN));
                            Criteria criteria = new Criteria(new Column("Brand", "BRAND_NAME"), text, QueryConstants.LIKE);
                            Criteria criteria1 = criteria.and(new Criteria(new Column("Product", "PRODUCT_ID"), productId, QueryConstants.LESS_THAN));
                            selectQuery.setCriteria(criteria1);
                            selectQuery.addSortColumn(new SortColumn("Product", "PRODUCT_ID", true));
                            selectQuery.setRange(new Range(0, 5));
                            try (DataSet resultSet = RelationalAPI.getInstance().executeQuery(selectQuery, connection)) {
                                while (resultSet.next()) {
                                    Product product = new Product();
                                    product.setProductId((Integer) resultSet.getValue(5));
                                    product.setName(resultSet.getAsString(1));
                                    product.setPrice((Double) resultSet.getValue(2));
                                    product.setBrand(BeanInstance.getBrandDAOType1().getBrandName((Integer) resultSet.getValue(3)));
                                    product.setCategory(BeanInstance.getCategoryDAOType1().getCategoryName((Integer) resultSet.getValue(4)));
                                    product.setImagePath(resultSet.getAsString(6));
                                    productSet.add(product);
                                }
                            } catch (QueryConstructionException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    } else {
                        try (Connection connection = DbUtil.getConnection()) {
                            SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Product"));
                            selectQuery.addSelectColumn(new Column("Product", "NAME"));
                            selectQuery.addSelectColumn(new Column("Product", "PRICE"));
                            selectQuery.addSelectColumn(new Column("Product", "BRAND_ID"));
                            selectQuery.addSelectColumn(new Column("Product", "CATEGORY_ID"));
                            selectQuery.addSelectColumn(new Column("Product", "PRODUCT_ID"));
                            selectQuery.addSelectColumn(new Column("Product", "IMAGE"));
                            selectQuery.addSelectColumn(new Column("Product", "SELLER_ID"));
                            selectQuery.addJoin(new Join("Product", "Brand", new String[]{"BRAND_ID"}, new String[]{"BRAND_ID"}, Join.INNER_JOIN));
                            Criteria criteria = new Criteria(new Column("Brand", "BRAND_NAME"), text, QueryConstants.LIKE);
                            Criteria criteria1 = criteria.and(new Criteria(new Column("Product", "PRODUCT_ID"), productId, QueryConstants.LESS_THAN));
                            selectQuery.setCriteria(criteria1);
                            selectQuery.addSortColumn(new SortColumn("Product", "PRODUCT_ID", true));
                            selectQuery.setRange(new Range(0, 5));
                            try (DataSet resultSet = RelationalAPI.getInstance().executeQuery(selectQuery, connection)) {
                                while (resultSet.next()) {
                                    Product product = new Product();
                                    product.setProductId((Integer) resultSet.getValue(5));
                                    product.setName(resultSet.getAsString(1));
                                    product.setPrice((Double) resultSet.getValue(2));
                                    product.setBrand(BeanInstance.getBrandDAOType1().getBrandName((Integer) resultSet.getValue(3)));
                                    product.setCategory(BeanInstance.getCategoryDAOType1().getCategoryName((Integer) resultSet.getValue(4)));
                                    product.setImagePath(resultSet.getAsString(6));
                                    productSet.add(product);
                                }
                            } catch (QueryConstructionException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    }
                    break;
                }
                case CATEGORY: {
                    if (navigation.equals(Enums.Navigation.PREVIOUS)) {
                        try (Connection connection = DbUtil.getConnection()) {
                            SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Product"));
                            selectQuery.addSelectColumn(new Column("Product", "NAME"));
                            selectQuery.addSelectColumn(new Column("Product", "PRICE"));
                            selectQuery.addSelectColumn(new Column("Product", "BRAND_ID"));
                            selectQuery.addSelectColumn(new Column("Product", "CATEGORY_ID"));
                            selectQuery.addSelectColumn(new Column("Product", "PRODUCT_ID"));
                            selectQuery.addSelectColumn(new Column("Product", "IMAGE"));
                            selectQuery.addSelectColumn(new Column("Product", "SELLER_ID"));
                            selectQuery.addJoin(new Join("Product", "Category", new String[]{"CATEGORY_ID"}, new String[]{"CATEGORY_ID"}, Join.INNER_JOIN));
                            Criteria criteria = new Criteria(new Column("Category", "NAME"), text, QueryConstants.LIKE);
                            Criteria criteria1 = criteria.and(new Criteria(new Column("Product", "PRODUCT_ID"), productId, QueryConstants.LESS_THAN));
                            selectQuery.setCriteria(criteria1);
                            selectQuery.addSortColumn(new SortColumn("Product", "PRODUCT_ID", true));
                            selectQuery.setRange(new Range(0, 5));
                            try (DataSet resultSet = RelationalAPI.getInstance().executeQuery(selectQuery, connection)) {
                                while (resultSet.next()) {
                                    Product product = new Product();
                                    product.setProductId((Integer) resultSet.getValue(5));
                                    product.setName(resultSet.getAsString(1));
                                    product.setPrice((Double) resultSet.getValue(2));
                                    product.setBrand(BeanInstance.getBrandDAOType1().getBrandName((Integer) resultSet.getValue(3)));
                                    product.setCategory(BeanInstance.getCategoryDAOType1().getCategoryName((Integer) resultSet.getValue(4)));
                                    product.setImagePath(resultSet.getAsString(6));
                                    productSet.add(product);
                                }
                            } catch (QueryConstructionException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    } else {
                        try (Connection connection = DbUtil.getConnection()) {
                            SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Product"));
                            selectQuery.addSelectColumn(new Column("Product", "NAME"));
                            selectQuery.addSelectColumn(new Column("Product", "PRICE"));
                            selectQuery.addSelectColumn(new Column("Product", "BRAND_ID"));
                            selectQuery.addSelectColumn(new Column("Product", "CATEGORY_ID"));
                            selectQuery.addSelectColumn(new Column("Product", "PRODUCT_ID"));
                            selectQuery.addSelectColumn(new Column("Product", "IMAGE"));
                            selectQuery.addSelectColumn(new Column("Product", "SELLER_ID"));
                            selectQuery.addJoin(new Join("Product", "Category", new String[]{"CATEGORY_ID"}, new String[]{"CATEGORY_ID"}, Join.INNER_JOIN));
                            Criteria criteria = new Criteria(new Column("Category", "NAME"), text, QueryConstants.LIKE);
                            Criteria criteria1 = criteria.and(new Criteria(new Column("Product", "PRODUCT_ID"), productId, QueryConstants.LESS_THAN));
                            selectQuery.setCriteria(criteria1);
                            selectQuery.addSortColumn(new SortColumn("Product", "PRODUCT_ID", true));
                            selectQuery.setRange(new Range(0, 5));
                            try (DataSet resultSet = RelationalAPI.getInstance().executeQuery(selectQuery, connection)) {
                                while (resultSet.next()) {
                                    Product product = new Product();
                                    product.setProductId((Integer) resultSet.getValue(5));
                                    product.setName(resultSet.getAsString(1));
                                    product.setPrice((Double) resultSet.getValue(2));
                                    product.setBrand(BeanInstance.getBrandDAOType1().getBrandName((Integer) resultSet.getValue(3)));
                                    product.setCategory(BeanInstance.getCategoryDAOType1().getCategoryName((Integer) resultSet.getValue(4)));
                                    product.setImagePath(resultSet.getAsString(6));
                                    productSet.add(product);
                                }
                            } catch (QueryConstructionException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    }
                }
            }
            return productSet;
        }
        catch (SQLException | RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

//    private void searchUtil(Set<Product> productList, SelectQuery selectQuery) throws SQLException {
//        try(DataSet resultSet = RelationalAPI.getInstance().executeQuery(selectQuery,DbUtil.getConnection()))
//        {
//            while (resultSet.next())
//            {
//                Product product = generateProduct(resultSet);
//                productList.add(product);
//            }
//        } catch (QueryConstructionException e) {
//            throw new RuntimeException(e);
//        }
//    }

//    public Set<Product> getProducts(int offset,String navOption) throws SQLException {
//        try (Connection connection = DbUtil.getConnection()) {
//            PreparedStatement statement;
//            if(navOption.equals("next"))
//            {
//                 statement= connection.prepareStatement("SELECT * from (SELECT  name,price,brand_id,category_id,product_id,image,seller_id from  product order by PRODUCT_ID) as p where PRODUCT_ID > ? limit 5 ");
//            } else if (navOption.equals("previous"))
//            {
//                 statement = connection.prepareStatement("SELECT * from(SELECT * from (SELECT  name,price,brand_id,category_id,product_id,image,seller_id from  product order by PRODUCT_ID desc ) as p where PRODUCT_ID < ? limit 5 ) as `p*`  order by PRODUCT_ID ");
//            }
//            else
//            {
//                statement=connection.prepareStatement("SELECT * from (SELECT  name,price,brand_id,category_id,product_id,image,seller_id from  product order by PRODUCT_ID) as p where PRODUCT_ID > ? limit 5");
//            }
//            statement.setInt(1,offset);
//                Set<Product> productSet = new LinkedHashSet<>();
//                try (ResultSet resultSet = statement.executeQuery()) {
//                    while (resultSet.next()) {
//                        Product product= generateProduct(resultSet);
//                        productSet.add(product);
//                    }
//                }
//                statement.close();
//                return productSet;
//            }
//        }


    public  Map<Integer,Product> getProductMap() throws SQLException {
        try (Connection connection = DbUtil.getConnection()) {
            SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Product"));
            selectQuery.addSelectColumn(new Column("Product", "NAME"));
            selectQuery.addSelectColumn(new Column("Product", "PRICE"));
            selectQuery.addSelectColumn(new Column("Product", "BRAND_ID"));
            selectQuery.addSelectColumn(new Column("Product", "CATEGORY_ID"));
            selectQuery.addSelectColumn(new Column("Product", "PRODUCT_ID"));
            selectQuery.addSelectColumn(new Column("Product", "IMAGE"));
            selectQuery.addSelectColumn(new Column("Product", "SELLER_ID"));
            Map<Integer, Product> productMap = new HashMap<>();
                try (DataSet resultSet = RelationalAPI.getInstance().executeQuery(selectQuery,connection)) {
                    while (resultSet.next()) {
                        Product product = generateProduct(resultSet);
                        productMap.put(product.getProductId(), product);
                    }
                } catch (QueryConstructionException e) {
                    throw new RuntimeException(e);
                }
            return productMap;

        }
    }

    private Product generateProduct(DataSet resultSet) throws SQLException {
        Product product = new Product();
        product.setProductId((Integer) resultSet.getValue(5));
        product.setSellerId((Integer) resultSet.getValue(7));
        product.setName(resultSet.getAsString(1));
        product.setPrice((Double) resultSet.getValue(2));
        product.setBrand(BeanInstance.getBrandDAOType1().getBrandName((Integer) resultSet.getValue(3)));
        product.setCategory(BeanInstance.getCategoryDAOType3().getCategoryName((Integer) resultSet.getValue(4)));
        product.setImagePath(resultSet.getAsString(6));
        return product;
    }

    public  Product getProduct(int productId) throws SQLException {
        try (Connection connection = DbUtil. getConnection()) {
            SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Product"));
            selectQuery.addSelectColumn(new Column("Product", "NAME"));
            selectQuery.addSelectColumn(new Column("Product", "PRICE"));
            selectQuery.addSelectColumn(new Column("Product", "BRAND_ID"));
            selectQuery.addSelectColumn(new Column("Product", "CATEGORY_ID"));
            selectQuery.addSelectColumn(new Column("Product", "PRODUCT_ID"));
            selectQuery.addSelectColumn(new Column("ProductDescription", "COLOR"));
            selectQuery.addSelectColumn(new Column("ProductDescription", "SIZE"));
            selectQuery.addSelectColumn(new Column("ProductDescription", "IDEAL_FOR"));
            selectQuery.addSelectColumn(new Column("Product", "IMAGE"));
            selectQuery.addJoin(new Join("Product","ProductDescription",new String[]{"PRODUCT_ID"},new String[]{"PRODUCT_ID"},Join.INNER_JOIN));
            selectQuery.setCriteria(new Criteria(new Column("Product","PRODUCT_ID"),productId,QueryConstants.EQUAL));
                try (DataSet resultSet = RelationalAPI.getInstance().executeQuery(selectQuery,connection))
                {
                    if (resultSet.next()) {
                        Product product = new Product();
                        product.setProductId(productId);
                        product.setName(resultSet.getAsString(1));
                        product.setPrice( (Double) resultSet.getValue(2) );
                        product.setBrand(BeanInstance.getBrandDAOType1().getBrandName((Integer) resultSet.getValue(3)));
                        product.setCategory(BeanInstance.getCategoryDAOType1().getCategoryName((Integer) resultSet.getValue(4)));
                        ProductDescription description = new ProductDescription();
                        description.setColor(resultSet.getAsString(6));
                        description.setSize((Integer) resultSet.getValue(7));
                        description.setCustomerGroup(resultSet.getAsString(8));
                        product.setImagePath(resultSet.getAsString(9));
                        product.setDescription(description);
                        return product;
                    }
                }
                catch (QueryConstructionException e)
                {
                    throw new RuntimeException(e);
                }
        }
        return null;
    }

    public  boolean updateProduct(Product product) throws SQLException
    {
        try {
            UpdateQuery updateQuery = new UpdateQueryImpl("Product");
            updateQuery.setUpdateColumn("SELLER_ID", product.getSellerId());
            updateQuery.setUpdateColumn("BRAND_ID", BeanInstance.getBrandDAOType1().getBrandId(product.getBrand()));
            updateQuery.setUpdateColumn("PRICE", product.getPrice());
            updateQuery.setUpdateColumn("CATEGORY_ID", BeanInstance.getCategoryDAOType1().getCategoryId(product.getCategory()));
            updateQuery.setUpdateColumn("NAME", product.getName());
            updateQuery.setUpdateColumn("PRODUCT_ID",product.getProductId());
            DataAccess.update(updateQuery);
            UpdateQuery updateQuery1 = new UpdateQueryImpl("ProductDescription");
            updateQuery1.setUpdateColumn("COLOR", product.getDescription().getColor());
            updateQuery1.setUpdateColumn("SIZE", product.getDescription().getSize());
            updateQuery1.setUpdateColumn("IDEAL_FOR", product.getDescription().getCustomerGroup());
            updateQuery1.setUpdateColumn("PRODUCT_ID",product.getProductId());
            DataAccess.update(updateQuery1);
        }
        catch (DataAccessException e)
        {
            throw new RuntimeException(e);
        }
        return false;
    }

    public  boolean removeProduct(int productId) {
        try {
            DeleteQuery deleteQuery = new DeleteQueryImpl("Product");
            deleteQuery.setCriteria(new Criteria(new Column("Product","PRODUCT_ID"),productId,QueryConstants.EQUAL));
            return DataAccess.delete(deleteQuery)==1;

        }
        catch (DataAccessException e)
        {
            throw new RuntimeException(e);
        }
    }

    public  String getImagePath(int productId) throws SQLException
    {
        try (Connection connection = DbUtil.getConnection()) {
            SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Product"));
            selectQuery.addSelectColumn(new Column("Product","IMAGE"));
            selectQuery.setCriteria(new Criteria(new Column("Product","PRODUCT_ID"),productId,QueryConstants.EQUAL));
            try(DataSet resultSet = RelationalAPI.getInstance().executeQuery(selectQuery,connection))
            {
                if (resultSet.next())
                {
                    return resultSet.getAsString("image");
                }
            }
            catch (QueryConstructionException e)
            {
                throw new RuntimeException(e);
            }
        }
        return null;
    }

//    @Override
//    public Integer getProductCount() throws SQLException
//    {
//        try(Connection connection= DbUtil.getConnection())
//        {
//            try(Statement statement= connection.createStatement())
//            {
//                try(ResultSet resultSet=statement.executeQuery("SELECT COUNT(PRODUCT_ID) from Product ;"))
//                {
//                    if(resultSet.next())
//                    {
//                        return resultSet.getInt(1);
//                    }
//                }
//
//            }
//        }
//        return null;
//    }
}
