package com.ecommerce.model.DbOperations;
import com.adventnet.db.api.RelationalAPI;
import com.adventnet.ds.query.*;
import com.adventnet.persistence.DataAccess;
import com.adventnet.persistence.DataAccessException;
import com.adventnet.persistence.Row;
import com.ecommerce.model.product.Brand;
import com.ecommerce.model.product.Category;
import java.sql.*;
import java.util.LinkedHashSet;
import java.util.Set;

public class CategoryDAOImpl implements CategoryDAO{
    public  Integer getCategoryId(String categoryName) throws SQLException {
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Category"));
        selectQuery.addSelectColumn(Column.getColumn("Category","CATEGORY_ID"));
        selectQuery.setCriteria(new Criteria(Column.getColumn("Category","CATEGORY_NAME"),categoryName,QueryConstants.EQUAL));
        try(DataSet dataSet= RelationalAPI.getInstance().executeQuery(selectQuery,DbUtil.getConnection()))
        {
            if (dataSet.next())
            {
                return (Integer) dataSet.getValue("CATEGORY_ID");
            }
        } catch (QueryConstructionException e) {
            throw new RuntimeException(e);
        }

        return null;
    }

    public  String getCategoryName(int category) throws SQLException {
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Category"));
        selectQuery.addSelectColumn(Column.getColumn("Category","CATEGORY_NAME"));
        selectQuery.setCriteria(new Criteria(Column.getColumn("Category","CATEGORY_ID"),category,QueryConstants.EQUAL));
        try(DataSet dataSet= RelationalAPI.getInstance().executeQuery(selectQuery,DbUtil.getConnection()))
        {
            if (dataSet.next())
            {
                return (String) dataSet.getValue("CATEGORY_NAME");
            }
        } catch (QueryConstructionException e) {
            throw new RuntimeException(e);
        }

        return null;
    }

    public  boolean addCategory(Category category) {
        InsertQueryImpl insertQuery = new InsertQueryImpl("Category",1);
        Row row = new Row("Category");
        row.set("CATEGORY_NAME",category.getName());
        insertQuery.addRow(row);
        try
        {
            DataAccess.add(insertQuery);
            return true;
        }
        catch (DataAccessException e)
        {
            return false;
        }
    }

    public Set<Category> getCategories() throws SQLException {
        Set<Category> categorySet = new LinkedHashSet<>();
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Category"));
        selectQuery.addSelectColumn(Column.getColumn("Category","CATEGORY_ID"));
        selectQuery.addSelectColumn(Column.getColumn("Category","CATEGORY_NAME"));
        selectQuery.addSortColumn(new SortColumn("Category","CATEGORY_ID",true));
        RelationalAPI relationalAPI = RelationalAPI.getInstance();
        try(DataSet dataSet= relationalAPI.executeQuery(selectQuery,DbUtil.getConnection()))
        {
            while (dataSet.next())
            {
                Category category = new Category(dataSet.getAsString("CATEGORY_NAME"));
                category.setCategoryId((Integer) dataSet.getValue("CATEGORY_ID"));
                categorySet.add(category);
            }
        }
        catch (QueryConstructionException e)
        {
            return null;
        }
        return categorySet;
    }

}
