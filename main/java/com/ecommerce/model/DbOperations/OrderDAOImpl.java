package com.ecommerce.model.DbOperations;
import com.adventnet.db.api.RelationalAPI;
import com.adventnet.ds.query.*;
import com.adventnet.persistence.*;
import com.ecommerce.model.util.Enums;
import com.ecommerce.model.product.Order;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OrderDAOImpl implements OrderDAO {

    public Set<Order> getOrders(int userId) throws SQLException {
        Set<Order> orderSet = new LinkedHashSet<>();
        try (Connection connection = DbUtil.getConnection()) {
            Integer userType = null;
            SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("User"));
            selectQuery.addSelectColumn(Column.getColumn("User", "USER_TYPE"));
            selectQuery.setCriteria(new Criteria(Column.getColumn("User", "USER_ID"), userId, QueryConstants.EQUAL));
            DataSet dataSet = RelationalAPI.getInstance().executeQuery(selectQuery,connection);
            if (dataSet.next()) {
                userType = (Integer) dataSet.getValue("USER_TYPE");
            }
            if (userType == Enums.UserType.BUYER.ordinal()) {
                SelectQuery selectQuery1 = new SelectQueryImpl(Table.getTable("Order"));
                List<Column> fields = new ArrayList<>();
                fields.add(new Column("Order", "ORDER_ID"));
                fields.add(new Column("Order", "ORDERED_DATE"));
                fields.add(new Column("Order", "DELIVERED_DATE"));
                fields.add(new Column("Order", "ORDER_STATUS"));
                selectQuery1.addSelectColumns(fields);
                selectQuery1.setCriteria(new Criteria(Column.getColumn("Order", "BUYER_ID"), userId, QueryConstants.EQUAL));
                selectQuery1.addSortColumn(new SortColumn("Order", "ORDER_ID", false));
                {
                    try (DataSet resultSet = RelationalAPI.getInstance().executeQuery(selectQuery1, connection)) {
                        while (resultSet.next()) {
                            Order order = new Order(userId);
                            Map<Integer, Integer> orderItems = new HashMap<>();
                            order.setOrderedDate(LocalDateTime.parse(resultSet.getAsString("ORDERED_DATE")));
                            order.setOrderId((Integer) resultSet.getValue("ORDER_ID"));
                            try
                            {
                                order.setDeliveredDate(LocalDateTime.parse(resultSet.getAsString("DELIVERED_DATE")));
                            } catch (NullPointerException e) {
                                order.setDeliveredDate(null);
                            }

                            if (((int) resultSet.getValue("ORDER_STATUS")) == 0)
                            {
                                order.setOrderStatus(Enums.OrderStatus.ORDERED);
                            }
                            else
                            {
                                order.setOrderStatus(Enums.OrderStatus.DELIVERED);
                            }
                            SelectQuery selectQuery2 = new SelectQueryImpl(Table.getTable("OrderItems"));
                            selectQuery2.addSelectColumn(Column.getColumn("OrderItems", "PRODUCT_ID"));
                            selectQuery2.addSelectColumn(Column.getColumn("OrderItems", "COUNT"));
                            selectQuery2.setCriteria(new Criteria(Column.getColumn("OrderItems", "ORDER_ID"), (int) resultSet.getValue("ORDER_ID"), QueryConstants.EQUAL));
                            RelationalAPI relationalAPI = RelationalAPI.getInstance();
                            try (DataSet resultSet1 = relationalAPI.executeQuery(selectQuery2, connection)) {
                                while (resultSet1.next()) {
                                    orderItems.put((Integer) resultSet1.getValue("PRODUCT_ID"), (Integer) resultSet1.getValue("COUNT"));
                                }
                                order.setItemsCount(orderItems);
                            }
                            orderSet.add(order);
                        }
                    }
                    return orderSet;
                }
            } else if (userType == Enums.UserType.SELLER.ordinal()) {
                SelectQuery selectQuery1 = new SelectQueryImpl(Table.getTable("OrderItems"));
                selectQuery1.addSelectColumn(Column.getColumn("Order", "ORDER_ID"));
                selectQuery1.addSelectColumn(Column.getColumn("Order", "BUYER_ID"));
                selectQuery1.addSelectColumn(Column.getColumn("OrderItems", "PRODUCT_ID"));
                selectQuery1.addSelectColumn(Column.getColumn("OrderItems", "COUNT"));
                selectQuery1.addSelectColumn(Column.getColumn("Order", "ORDER_STATUS"));
                selectQuery1.addJoin(new Join("OrderItems", "Order", new String[]{"ORDER_ID"}, new String[]{"ORDER_ID"}, Join.INNER_JOIN));
                selectQuery1.addJoin(new Join("OrderItems", "Product", new String[]{"PRODUCT_ID"}, new String[]{"PRODUCT_ID"}, Join.INNER_JOIN));
                selectQuery1.setCriteria(new Criteria(new Column("Product", "SELLER_ID"), userId, QueryConstants.EQUAL));
                try (DataSet resultSet = RelationalAPI.getInstance().executeQuery(selectQuery1, connection)) {
                    while (resultSet.next()) {
                        Order order = new Order((Integer) resultSet.getValue(2));
                        order.setOrderId((Integer) resultSet.getValue(1));
                        Map<Integer, Integer> itemCount = new HashMap<>();
                        if (((int) resultSet.getValue(5)) == 0) {
                            order.setOrderStatus(Enums.OrderStatus.ORDERED);
                        } else {
                            order.setOrderStatus(Enums.OrderStatus.DELIVERED);
                        }
                        itemCount.put((Integer) resultSet.getValue(3), (Integer) resultSet.getValue(4));
                        order.setItemsCount(itemCount);
                        orderSet.add(order);
                    }
                } catch (QueryConstructionException ex) {
                    throw new RuntimeException(ex);
                }
                return orderSet;
            }
            else if (userType == Enums.UserType.DELIVERY_AGENT.ordinal())
            {
                SelectQuery selectQuery1 = new SelectQueryImpl(Table.getTable("OrderDelivery"));
                selectQuery1.addSelectColumn(Column.getColumn("OrderDelivery", "ORDER_ID"));
                selectQuery1.setCriteria(new Criteria(new Column("OrderDelivery", "DELIVERY_AGENT_ID"), userId, QueryConstants.EQUAL));
                selectQuery1.addSortColumn(new SortColumn("OrderDelivery", "ORDER_ID", true));
                try (DataSet resultSet = RelationalAPI.getInstance().executeQuery(selectQuery1, connection)) {
                    while (resultSet.next())
                    {
                        int orderId = (int) resultSet.getValue("ORDER_ID");
                        orderSet.add(getOrder(orderId));
                    }
                    return orderSet;
                }
                catch (QueryConstructionException ex)
                {
                    throw new RuntimeException(ex);
                }
            }

        } catch (QueryConstructionException e) {
            throw new RuntimeException(e);
        }
        return orderSet;
    }

    public Order getOrder(int orderId) throws SQLException {
        try (Connection connection = DbUtil.getConnection()) {
            SelectQuery selectQuery = new SelectQueryImpl(Table.getTable("Order"));
            selectQuery.addSelectColumn(Column.getColumn("Order", "ORDER_ID"));
            selectQuery.addSelectColumn(Column.getColumn("Order", "BUYER_ID"));
            selectQuery.addSelectColumn(Column.getColumn("Order", "ORDERED_DATE"));
            selectQuery.addSelectColumn(Column.getColumn("Order", "DELIVERED_DATE"));
            selectQuery.addSelectColumn(Column.getColumn("Order", "ORDER_STATUS"));
            selectQuery.setCriteria(new Criteria(Column.getColumn("Order", "ORDER_ID"), orderId, QueryConstants.EQUAL));
            Logger.getAnonymousLogger().log(Level.SEVERE,"In getOrder() method");
            try (DataSet resultSet = RelationalAPI.getInstance().executeQuery(selectQuery, connection)) {
                if (resultSet.next()) {
                    Order order = new Order((Integer) resultSet.getValue("BUYER_ID"));
                    Map<Integer, Integer> orderItems = new HashMap<>();
                    order.setOrderedDate(LocalDateTime.parse((resultSet.getAsString("ORDERED_DATE"))));
                    order.setOrderId((Integer) resultSet.getValue("ORDER_ID"));
                    try {
                        order.setDeliveredDate(LocalDateTime.parse((resultSet.getAsString("DELIVERED_DATE"))));
                    } catch (NullPointerException e) {
                        order.setOrderStatus(null);
                    }

                    if ((int) resultSet.getValue("ORDER_STATUS") == 0) {
                        order.setOrderStatus(Enums.OrderStatus.ORDERED);
                    } else {
                        order.setOrderStatus(Enums.OrderStatus.DELIVERED);
                    }
                    SelectQuery selectQuery1 = new SelectQueryImpl(Table.getTable("OrderItems"));
                    selectQuery1.addSelectColumn(Column.getColumn("OrderItems", "PRODUCT_ID"));
                    selectQuery1.addSelectColumn(Column.getColumn("OrderItems", "COUNT"));
                    selectQuery1.setCriteria(new Criteria(Column.getColumn("OrderItems", "ORDER_ID"), orderId, QueryConstants.EQUAL));
                    try (DataSet resultSet1 = RelationalAPI.getInstance().executeQuery(selectQuery1, connection)) {
                        while (resultSet1.next()) {
                            orderItems.put((Integer) resultSet1.getValue("PRODUCT_ID"), (Integer) resultSet1.getValue("COUNT"));
                        }
                        order.setItemsCount(orderItems);
                    }
                    return order;
                }
            } catch (QueryConstructionException e) {
                throw new RuntimeException(e);
            }
        }
        return null;

    }


    public void addOrder(Order order) throws SQLException
    {
        try
        {
            String timestamp = String.valueOf(LocalDateTime.now());
            DataObject dataObject = new WritableDataObject();
            Row row = new Row("Order");
            row.set("BUYER_ID",order.getUserId());
            row.set("ORDERED_DATE",timestamp);
            row.set("DELIVERED_DATE",null);
            dataObject.addRow(row);
            DataAccess.add(dataObject);
            Integer orderId = row.getInt("ORDER_ID");
            dataObject.deleteRow(row);
            Set<Integer> keySet = order.getItemsCount().keySet();
            Logger.getAnonymousLogger().log(Level.SEVERE, Arrays.toString(keySet.toArray()));
            for (Integer productId : keySet)
            {
                Row row1= new Row("OrderItems");
                row1.set("ORDER_ID",orderId);
                row1.set("PRODUCT_ID",productId);
                row1.set("COUNT",order.getItemsCount().get(productId));
                dataObject.addRow(row1);
                BeanInstance.getCartDAOType3().removeCartEntry(BeanInstance.getCartDAOType3().getCartId(order.getUserId()), productId);
            }
            Row row1= new Row("OrderDelivery");
            row1.set("ORDER_ID",orderId);
            row1.set("DELIVERY_AGENT_ID",4);
            dataObject.addRow(row1);
            DataAccess.add(dataObject);
        }
        catch (DataAccessException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void changeOrderStatus(int orderId) throws SQLException {
        try  {
            String timeStamp = String.valueOf(LocalDateTime.now());
            UpdateQuery updateQuery = new UpdateQueryImpl("Order");
            updateQuery.setUpdateColumn("ORDER_STATUS",1);
            updateQuery.setUpdateColumn("DELIVERED_DATE",timeStamp);
            updateQuery.setCriteria(new Criteria(Column.getColumn("Order","ORDER_ID"),orderId,QueryConstants.EQUAL));
            DataAccess.update(updateQuery);
        }
        catch (DataAccessException e)
        {
            throw new RuntimeException(e);
        }
    }


//    private Integer getOrderId(int userId, String timestamp, Connection connection) throws SQLException {
//        try (PreparedStatement statement = connection.prepareStatement("SELECT ORDER_ID from `Order` where BUYER_ID=? and ORDERED_DATE=?")) {
//            statement.setInt(1, userId);
//            statement.setString(2, timestamp);
//            try (ResultSet resultSet = statement.executeQuery()) {
//                if (resultSet.next()) {
//                    return resultSet.getInt(1);
//                }
//            }
//        }
//        return null;
//    }

}