package com.ecommerce.model.DbOperations;

import com.ecommerce.model.product.Brand;

import java.sql.SQLException;
import java.util.Set;

public interface BrandDAO {
    Set<Brand> getBrands() throws SQLException;
    Integer getBrandId(String brandName) throws SQLException;
    String getBrandName(int brandId) throws SQLException;
    boolean addBrand(Brand brand) throws SQLException;
}
