package com.ecommerce.model.DbOperations;
import com.adventnet.persistence.DataAccessException;
import com.ecommerce.model.user.*;
import java.sql.SQLException;
import java.util.Set;
public interface UserDAO {

    User isAuthenticatedUser(String userName, String password) throws SQLException;
    boolean addUser(User user) throws SQLException, DataAccessException;
    Set<Seller> getSellers() throws SQLException;
    Set<DeliveryAgent> getDeliveryAgents() throws SQLException;
    boolean removeUser(int userId) throws SQLException;
    Address getUserAddress(int userId) throws SQLException;
    boolean registerBank(BankAccount bankAccount) throws SQLException;
}
