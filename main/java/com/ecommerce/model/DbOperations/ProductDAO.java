package com.ecommerce.model.DbOperations;

import com.ecommerce.model.product.Product;
import com.ecommerce.model.util.Enums;
import com.ecommerce.model.util.FilterCriteria;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Set;

public interface ProductDAO {

    boolean addProduct(Product product) throws SQLException;
    Set<Product> getProducts(int userId) throws SQLException;
    Set<Product> getProducts() throws SQLException;
    Set<Product> getProducts(FilterCriteria filterCriteria, int offset, Enums.Navigation navOption) throws SQLException;

   // Set<Product> getProducts(int offset,String navOption) throws SQLException;

    Map<Integer,Product> getProductMap() throws SQLException;//productId vs Product
    Product getProduct(int productId) throws SQLException;
    boolean updateProduct(Product product) throws SQLException;
    boolean removeProduct(int productId) throws SQLException;
    Double getPrice(int productId) throws SQLException;
    String getImagePath(int productId) throws SQLException, IOException;

}
