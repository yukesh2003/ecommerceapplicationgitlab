package com.ecommerce.model.product;
public class ProductDescription
{
    private int size;
    private String color;
    private String customerGroup;

    public int getSize()
    {
        return size;
    }

    public void setSize(int size)
    {
        this.size = size;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public String getCustomerGroup()
    {
        return customerGroup;
    }

    public void setCustomerGroup(String customerGroup)
    {
        this.customerGroup = customerGroup;
    }
}
