package com.ecommerce.model.util;
public  class FilterCriteria {

private FilterType type;
private String value;
public enum FilterType
{
    CATEGORY, NAME, BRAND
}
 public FilterCriteria(FilterType type, String value)
 {
     this.type=type;
     this.value=value;
 }

    public FilterType getType() {
        return type;
    }
    public String getValue() {
        return value;
    }
}