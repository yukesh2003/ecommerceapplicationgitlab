package com.ecommerce.controller;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
public class SearchAction extends ActionSupport {
  private  String text,option;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    @Override
    public String execute() throws ServletException, IOException {
        HttpServletRequest request= ServletActionContext.getRequest();
        request.getSession().setAttribute("text",text);
        request.getSession().setAttribute("option",option);
        return SUCCESS;
    }
}
