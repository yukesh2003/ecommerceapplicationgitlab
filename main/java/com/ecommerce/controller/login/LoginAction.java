package com.ecommerce.controller.login;
import com.ecommerce.model.DbOperations.BeanInstance;
import com.ecommerce.model.user.*;
import com.ecommerce.model.util.Enums;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginAction extends ActionSupport {
    private String userName;
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String execute() throws IOException {
        HttpServletRequest request= ServletActionContext.getRequest();
        HttpServletResponse response=ServletActionContext.getResponse();
        try {
            User user= BeanInstance.getUserDAOType1().isAuthenticatedUser(userName,password);
            if(user!=null)
            {
                request.getSession().setAttribute("user",user);
                Enums.UserType userType= user.getUserType();
                if(userType.equals(Enums.UserType.BUYER)){
                    Logger.getAnonymousLogger().log(Level.SEVERE,"BUYER 1 ST TIME ");
                    return "buyer";}
                else if (userType.equals(Enums.UserType.SELLER)) {return "seller";}
                else if (userType.equals(Enums.UserType.DELIVERY_AGENT)) {return "deliveryAgent";}
                else if(userType.equals(Enums.UserType.ADMINISTRATOR)){return "admin";}
            }
            else
            {
                PrintWriter out = response.getWriter();
                response.setContentType("text/html");
                out.println( "<div class='alert' style=\"color: crimson;background-color: azure;align-items: center;display: block;align-self:center;cursor: pointer;font-size:150%;\"> ");
                out.println("<span class='closebtn' onclick='this.parentElement.style.display=\"none\";'>&times;</span>" );
                out.println("<strong>OOPS!</strong> Invalid credentials");
                out.println("</div> ");
                request.getRequestDispatcher("login.jsp").include(request,response);
            }
        }
        catch (SQLException | ServletException e)
        {
            response.getWriter().println(e.getMessage());
        }
        return null;
    }
}
