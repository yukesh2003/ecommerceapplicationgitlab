package com.ecommerce.controller.login;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import javax.servlet.http.*;
import java.io.IOException;

public class SignOutAction extends ActionSupport {
    @Override
    public String execute() throws IOException {
            HttpServletRequest request= ServletActionContext.getRequest();
            request.getSession().setAttribute("user",null);
            request.getSession(false).invalidate();
            return SUCCESS;
    }
}
