package com.ecommerce.controller.adminOps;

import com.ecommerce.model.DbOperations.BeanInstance;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;

public class VerifyCategoryAction extends ActionSupport {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
   public String  execute() throws  IOException {
        HttpServletResponse response=ServletActionContext.getResponse();
        try
        {
            Integer categoryId= BeanInstance.getCategoryDAOType1().getCategoryId( name);
            response.getWriter().println(categoryId == null);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        return SUCCESS;
    }
}





