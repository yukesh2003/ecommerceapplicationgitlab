package com.ecommerce.controller.adminOps;
import com.adventnet.persistence.DataAccessException;
import com.ecommerce.model.DbOperations.BeanInstance;
import com.ecommerce.model.user.Address;
import com.ecommerce.model.user.Login;
import com.ecommerce.model.user.Seller;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

public class AddSellerAction extends ActionSupport {
    private String email,phone,name,password,state,district,locality,pincode;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    @Override
    public String execute() throws ServletException, IOException
    {
        HttpServletResponse response= ServletActionContext.getResponse();
        PrintWriter out=response.getWriter();
        response.setContentType("text/html");
        Seller seller=new Seller(email);
        seller.setPhone(phone);
        seller.setName(name);
        Address address=new Address();
        address.setLocality(locality);
        address.setDistrict(district);
        address.setState(state);
        address.setPinCode(Integer.valueOf(pincode));
        seller.setAddress(address);
        Login login=new Login(password);
        seller.setLogin(login);
        boolean status;
        try
        {
            status = BeanInstance.getUserDAOType2().addUser(seller);
            if(status)
            {   out.println("<script type=\"text/javascript\" s>");
                out.println("alert('Seller Added Successfully..');");
                out.println("</script>");
                return SUCCESS;
            }
            else
            {
                out.println("<script type=\"text/javascript\" s>");
                out.println("alert('Something went wrong...Seller cannot be added');");
                out.println("</script>");
                return ERROR;
            }
        }
        catch (SQLException e)
        {
            out.println("<script type=\"text/javascript\" s>");
            out.println("alert('something went wrong..');");
            out.println("</script>");
            return ERROR;
        } catch (DataAccessException e) {
            throw new RuntimeException(e);
        }

    }
}
