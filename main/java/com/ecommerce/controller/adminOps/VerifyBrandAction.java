package com.ecommerce.controller.adminOps;
import com.ecommerce.model.DbOperations.BeanInstance;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;

public class VerifyBrandAction extends ActionSupport {
   private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String execute() throws IOException {
        verifyBrand();
        return SUCCESS;
    }
    private void verifyBrand() throws IOException {
        try
        {
            HttpServletResponse response=ServletActionContext.getResponse();
            Integer brandId= BeanInstance.getBrandDAOType1().getBrandId(name);
            response.getWriter().println(brandId == null);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }

    }
}
