package com.ecommerce.controller.adminOps;
import com.ecommerce.model.DbOperations.BeanInstance;
import com.ecommerce.model.product.Brand;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddBrandAction extends ActionSupport {
    private String name;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    @Override
    public String execute() throws IOException {
        addBrand(name);
        return SUCCESS;
    }
    private void addBrand(String name) throws IOException {
        Logger.getAnonymousLogger().log(Level.SEVERE,"BRAND NAME IS "+name);
        HttpServletResponse response=ServletActionContext.getResponse();
        Brand brand=new Brand(name);
        boolean status ;
        PrintWriter out=response.getWriter();
        response.setContentType("plain/text");
        try
        {
            status= BeanInstance.getBrandDAOType1().addBrand(brand);
            out.println(status);
        }
        catch (SQLException e)
        {
           addActionMessage("Can't add Brand");
        }
    }

}
