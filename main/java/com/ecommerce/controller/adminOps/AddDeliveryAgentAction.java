package com.ecommerce.controller.adminOps;
import com.adventnet.persistence.DataAccessException;
import com.ecommerce.model.DbOperations.BeanInstance;
import com.ecommerce.model.user.Address;
import com.ecommerce.model.user.DeliveryAgent;
import com.ecommerce.model.user.Login;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class AddDeliveryAgentAction extends ActionSupport {
    private String email,phone_no,name,password,state,district,locality,pincode;

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public void setLocality(String locality)
    {
        this.locality = locality;
    }

    public void setPincode(String pincode)
    {
        this.pincode = pincode;
    }

    @Override
    public String execute() throws IOException
    {
        HttpServletResponse response = ServletActionContext.getResponse();
        response.setContentType("text/html");
        DeliveryAgent deliveryAgent = new DeliveryAgent(email);
        deliveryAgent.setPhone(phone_no);
        deliveryAgent.setName(name);
        Login login = new Login(password);
        deliveryAgent.setLogin(login);
        Address address = new Address();
        address.setState(state);
        address.setDistrict(district);
        address.setLocality(locality);
        address.setPinCode(Integer.valueOf(pincode));
        deliveryAgent.setAddress(address);
        boolean status;
        try {
            status = BeanInstance.getUserDAOType2().addUser(deliveryAgent);
            if (status)
            {
                addActionMessage("Delivery Agent Added Successfully");
                return SUCCESS;
            }
            else
            {
                addActionMessage("cannot add Delivery Agent");
                return ERROR;
            }
        }
        catch (SQLException e)
        {
           addActionMessage("cannot add Delivery Agent");
            return ERROR;
        }
        catch (DataAccessException e)
        {
            throw new RuntimeException(e);
        }
    }
}
