package com.ecommerce.controller.adminOps;
import com.ecommerce.model.DbOperations.BeanInstance;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

public class RemoveUserAction extends ActionSupport {
    private String userId;

    public void setUserId(String userId) {
        this.userId=userId;
    }

    @Override
   public String execute()throws IOException {
        HttpServletResponse response= ServletActionContext.getResponse();
        PrintWriter out=response.getWriter();
        try
        {
            if(BeanInstance.getUserDAOType2().removeUser(Integer.parseInt(userId)))
            {
                out.println("<script type=\"text/javascript\" s>");
                out.println("alert('User Removed Successfully..');");
                out.println("</script>");
                return SUCCESS;
            }
            else {
                out.println("<script type=\"text/javascript\" s>");
                out.println("alert('something went wrong..');");
                out.println("</script>");
                return ERROR;
            }

        }
        catch (NumberFormatException|SQLException e)
        {
            out.println("<script type=\"text/javascript\" s>");
            out.println("alert('something went wrong..');");
            out.println("</script>");
            return ERROR;
        }

    }
}
