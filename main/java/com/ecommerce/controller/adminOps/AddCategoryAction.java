package com.ecommerce.controller.adminOps;
import com.ecommerce.model.DbOperations.BeanInstance;
import com.ecommerce.model.product.Category;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddCategoryAction extends ActionSupport {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String execute ()throws  IOException {
        HttpServletResponse response=ServletActionContext.getResponse();
        Category category=new Category(name);
        boolean status ;
        PrintWriter out=response.getWriter();
        try
        {
            status= BeanInstance.getCategoryDAOType2().addCategory(category);
            out.println(status);
        }
        catch (SQLException e)
        {
            Logger.getAnonymousLogger().log(Level.SEVERE,e.getMessage());
        }
        return SUCCESS;
    }
}
