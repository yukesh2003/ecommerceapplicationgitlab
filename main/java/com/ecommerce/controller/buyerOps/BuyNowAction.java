package com.ecommerce.controller.buyerOps;
import com.adventnet.mfw.bean.BeanUtil;
import com.ecommerce.model.DbOperations.BeanInstance;
import com.ecommerce.model.DbOperations.CartDAOImpl;
import com.ecommerce.model.DbOperations.OrderDAOImpl;
import com.ecommerce.model.product.Cart;
import com.ecommerce.model.product.Order;
import com.ecommerce.model.user.Buyer;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BuyNowAction extends ActionSupport {
    private String productId;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Override
    public String execute() throws ServletException, IOException {
        HttpServletRequest request= ServletActionContext.getRequest();

        Buyer buyer=((Buyer) request.getSession().getAttribute("user"));
        try {
            buyer.setAddress(BeanInstance.getUserDAOType3().getUserAddress(buyer.getUserId()));
            Cart cart= BeanInstance.getCartDAOType3().getCart(buyer.getUserId());
            int count= cart.getItems().get(Integer.parseInt(productId));
            Order order=new Order(buyer.getUserId());
            Map<Integer,Integer> orderItems=new HashMap<>();
            orderItems.put(Integer.parseInt(productId),count);
            order.setItemsCount(orderItems);
            order.setAddress(buyer.getAddress());
            BeanInstance.getOrderDAOType2().addOrder(order);
            addActionMessage("Order Placed Successfully");
            return SUCCESS;
        }
        catch (SQLException e)
        {
            addActionMessage("Cannot place Order");
            Logger.getAnonymousLogger().log(Level.SEVERE,e.getMessage());
            return ERROR;
        }
    }
}
