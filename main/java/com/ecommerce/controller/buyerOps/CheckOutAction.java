package com.ecommerce.controller.buyerOps;

import com.ecommerce.model.DbOperations.BeanInstance;
import com.ecommerce.model.DbOperations.CartDAOImpl;
import com.ecommerce.model.DbOperations.OrderDAOImpl;
import com.ecommerce.model.product.Cart;
import com.ecommerce.model.product.Order;
import com.ecommerce.model.user.Buyer;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CheckOutAction extends ActionSupport {

    @Override
    public String execute() throws ServletException, IOException {
        HttpServletRequest request= ServletActionContext.getRequest();
        HttpServletResponse response=ServletActionContext.getResponse();
        PrintWriter out= response.getWriter();
        Buyer buyer=((Buyer)request.getSession().getAttribute("user"));
        try {
            Cart cart = BeanInstance.getCartDAOType3().getCart(buyer.getUserId());
            Map<Integer,Integer> dbItemsMap=cart.getItems();
            Set<Integer> keyset=dbItemsMap.keySet();
            Order order=new Order(buyer.getUserId());
            Map<Integer, Integer> orderItems=new HashMap<>();
            for (int key: keyset)
            {
                orderItems.put(key,dbItemsMap.get(key));
            }
            order.setItemsCount(orderItems);
            order.setAddress(buyer.getAddress());
            BeanInstance.getOrderDAOType2().addOrder(order);
            out.println("<script type=\"text/javascript\" s>");
            out.println("alert('Order Placed Successfully');");
            out.println("location='buyerHome';");
            out.println("</script>");
            return SUCCESS;
        } catch (SQLException e) {
            addActionMessage("Order Placed Successfully");
            Logger.getAnonymousLogger().log(Level.SEVERE,e.getMessage());
            return ERROR;
        }
    }
}
