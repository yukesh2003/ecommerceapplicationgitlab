package com.ecommerce.controller.buyerOps;
import com.ecommerce.model.DbOperations.BeanInstance;
import com.ecommerce.model.user.Buyer;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RefreshCartPriceAction extends ActionSupport {
    @Override
    public String execute() throws  IOException {
        HttpServletRequest request= ServletActionContext.getRequest();
        HttpServletResponse response=ServletActionContext.getResponse();
        Buyer buyer=(Buyer) request.getSession().getAttribute("user");
        try {
            response.setContentType("text/plain");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().println(BeanInstance.getCartDAOType1().getCartPrice(buyer.getUserId()));
        } catch (SQLException e) {
            Logger.getAnonymousLogger().log(Level.SEVERE,e.getMessage());
        }
        return null;
    }
}
