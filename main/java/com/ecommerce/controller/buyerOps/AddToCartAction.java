package com.ecommerce.controller.buyerOps;
import com.ecommerce.model.DbOperations.BeanInstance;
import com.ecommerce.model.DbOperations.CartDAOImpl;
import com.ecommerce.model.user.Buyer;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

public class AddToCartAction extends ActionSupport {
    private String productId,count;

    public void setProductId(String productId) {
        this.productId = productId;
    }
    public void setCount(String count) {
        this.count = count;
    }

    @Override
    public String execute() throws ServletException, IOException
    {
        HttpServletRequest request= ServletActionContext.getRequest();
        HttpServletResponse response=ServletActionContext.getResponse();
        PrintWriter out= response.getWriter();
        int userId=((Buyer)request.getSession().getAttribute("user")).getUserId();
        try
        {
            int quantity= BeanInstance.getCartDAOType1().addToCart(userId,Integer.parseInt(productId), Integer.parseInt(count));
            out.println(quantity);
        }
        catch (SQLException e)
        {
            addActionMessage("cannot add to cart");
        }
        return null;
    }
}
