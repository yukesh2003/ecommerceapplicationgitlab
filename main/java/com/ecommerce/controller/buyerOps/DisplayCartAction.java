package com.ecommerce.controller.buyerOps;
import com.ecommerce.model.DbOperations.BeanInstance;
import com.ecommerce.model.product.Product;
import com.ecommerce.model.user.User;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import java.util.HashMap;
import java.util.Map;

public class DisplayCartAction  extends ActionSupport {
    User user;
    Map<Integer,Integer> cartMap;
    Map<Integer, Product> productMap;

    public Map<Integer, Product> getProductMap() {
        return productMap;
    }

    public User getUser() {
        return user;
    }

    public Map<Integer, Integer> getCartMap() {
        return cartMap;
    }

    @Override
    public String execute() throws Exception {
        user=(User) ServletActionContext.getRequest().getSession(false).getAttribute("user");
        cartMap = BeanInstance.getCartDAOType1().getCart(user.getUserId()).getItems();
        productMap=new HashMap<>();
        for (Integer productId: cartMap.keySet())
        {
            productMap.put(productId, BeanInstance.getProductDAOType3().getProduct(productId));
        }
        return SUCCESS;
    }
}
