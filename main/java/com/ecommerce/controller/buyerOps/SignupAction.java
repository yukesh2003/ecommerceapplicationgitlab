package com.ecommerce.controller.buyerOps;
import com.adventnet.persistence.DataAccessException;
import com.ecommerce.model.DbOperations.BeanInstance;
import com.ecommerce.model.user.Address;
import com.ecommerce.model.user.Buyer;
import com.ecommerce.model.user.Login;
import com.ecommerce.model.util.Enums;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
public class SignupAction extends ActionSupport {
   private String email,phoneNo,name,gender,password,state,district,locality,pincode;

    public void setEmail(String email) {
        this.email = email;
    }


    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public void setName(String name) {
        this.name = name;
    }



    public void setGender(String gender) {
        this.gender = gender;
    }



    public void setPassword(String password) {
        this.password = password;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    @Override
    public String execute() throws ServletException, IOException {
        HttpServletResponse response=ServletActionContext.getResponse();
        response.setContentType("text/html");
        Buyer buyer=new Buyer(email);
        buyer.setPhone(phoneNo);
        buyer.setName(name);

        if(Integer.parseInt(gender)==0)
        {
            buyer.setGender(Enums.Gender.MALE);
        }
        else if(Integer.parseInt(gender)==1)
        {
            buyer.setGender(Enums.Gender.FEMALE);
        }
        else
        {
            buyer.setGender(Enums.Gender.TRANSGENDER);
        }
        Login login=new Login(password);
        buyer.setLogin(login);
        Address address=new Address();
        address.setLocality(locality);
        address.setDistrict(district);
        address.setState(state);
        address.setPinCode(Integer.valueOf(pincode));
        buyer.setAddress(address);
       boolean signupStatus;
        try
        {
            signupStatus = BeanInstance.getUserDAOType2().addUser(buyer);
            if(signupStatus)
            {
                addActionMessage("REGISTRATION SUCCESSFUL");
                return SUCCESS;
            }
            else
            {
                addActionMessage("Registration failed");
                return ERROR;
            }
        }
        catch (SQLException e)
        {
            Logger.getAnonymousLogger().log(Level.SEVERE,e.getMessage());
            addActionMessage("Registration failed");
            return ERROR;
        } catch (DataAccessException e) {
            throw new RuntimeException(e);
        }

    }
}
