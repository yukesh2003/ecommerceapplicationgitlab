package com.ecommerce.controller;
import com.ecommerce.model.DbOperations.BeanInstance;
import com.ecommerce.model.DbOperations.OrderDAOImpl;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ChangeOrderStatusAction extends ActionSupport {
   private String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String execute() throws ServletException, IOException {
        HttpServletResponse response= ServletActionContext.getResponse();
        try {
            BeanInstance.getOrderDAOType2().changeOrderStatus(Integer.parseInt(orderId));
            response.getWriter().println("DELIVERED");
        } catch (SQLException e) {
            response.getWriter().println(e.getMessage());
            Logger.getAnonymousLogger().log(Level.SEVERE,e.getMessage());
        }
        return null;
    }
}
