package com.ecommerce.controller.Filter;
import com.ecommerce.model.user.User;
import com.ecommerce.model.util.Enums;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import org.apache.struts2.ServletActionContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
public class AdminInterceptor implements Interceptor {
    @Override
    public void destroy() {

    }

    @Override
    public void init() {

    }

    @Override
    public String intercept(ActionInvocation actionInvocation) throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        HttpServletResponse response=ServletActionContext.getResponse();
        try
        {
            if (request.getSession(false).getAttribute("user") != null)
            {
                if (((User) request.getSession(false).getAttribute("user")).getUserType() != Enums.UserType.ADMINISTRATOR)
                {
                    request.getRequestDispatcher("accessError.jsp").forward(request, response);
                }
                else
                {
                    actionInvocation.invoke();
                }
            }
            else
            {
                return "login.jsp";
            }
        }
        catch (NullPointerException e)
        {
            Logger.getAnonymousLogger().log(Level.SEVERE,e.getMessage());
            return "login.jsp";
        }
        return null;
    }
}
