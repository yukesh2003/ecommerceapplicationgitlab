package com.ecommerce.controller.Filter;
import com.ecommerce.model.user.User;
import com.ecommerce.model.util.Enums;
import org.apache.struts2.ServletActionContext;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
public class AdminFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = ServletActionContext.getRequest();
        HttpServletResponse response=ServletActionContext.getResponse();
        try
        {
            if (request.getSession(false).getAttribute("user") != null)
            {
                if (((User) request.getSession(false).getAttribute("user")).getUserType() != Enums.UserType.ADMINISTRATOR)
                {
                    request.getRequestDispatcher("accessError.jsp").forward(request, response);
                }
                else
                {
                  filterChain.doFilter(request,response);
                }
            }
            else
            {
              response.sendRedirect("login.jsp");

            }
        }
        catch (NullPointerException e)
        {
            Logger.getAnonymousLogger().log(Level.SEVERE,e.getMessage());
            response.sendRedirect("login.jsp");
        }
    }

    @Override
    public void destroy()
    {}

}
