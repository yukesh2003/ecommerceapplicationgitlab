package com.ecommerce.controller.Filter;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
public class ValidUserRestrictionFilter implements Filter {
    public void init(FilterConfig config)  {
    }

    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)  {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response=(HttpServletResponse) resp;
        PrintWriter out= null;
        try{
            out=response.getWriter();
            if(request.getSession().getAttribute("user") != null)
            {
                request.getRequestDispatcher("accessError.jsp").forward(request,response);
            }
            else if(request.getSession().getAttribute("user") == null)
            {
                chain.doFilter(request,response);
            }

        }
        catch (Exception e)
        {
            Logger.getAnonymousLogger().log(Level.SEVERE,e.getMessage());
            request.getSession().invalidate();
            out.println("<script type=\"text/javascript\" s>");
            out.println("alert('Something Gone Wrong in VURF');");
            out.println("location='index.html';");
            out.println("</script>");
        }
    }
}
