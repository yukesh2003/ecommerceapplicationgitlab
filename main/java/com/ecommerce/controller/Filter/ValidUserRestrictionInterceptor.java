package com.ecommerce.controller.Filter;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import org.apache.struts2.ServletActionContext;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ValidUserRestrictionInterceptor implements Interceptor {
    @Override
    public void destroy() {

    }

    @Override
    public void init() {

    }

    @Override
    public String intercept(ActionInvocation actionInvocation) throws Exception {

        HttpServletRequest request = ServletActionContext.getRequest();
        HttpServletResponse response=ServletActionContext.getResponse();
        PrintWriter out=response.getWriter();
        try
        {
            if(request.getSession().getAttribute("user")!=null)
        {
            request.getRequestDispatcher("accessError.jsp").forward(request,response);
        }
        else if(request.getSession().getAttribute("user")==null)
        {
            actionInvocation.invoke();
        }

        }
        catch (IOException | ServletException e)
        {
            Logger.getAnonymousLogger().log(Level.SEVERE,e.getMessage());
            request.getSession().invalidate();
            out.println("<script type=\"text/javascript\" s>");
            out.println("alert('Something Gone Wrong in VURI');");
            out.println("location='index.html';");
            out.println("</script>");
        }

        return null;
    }
}
