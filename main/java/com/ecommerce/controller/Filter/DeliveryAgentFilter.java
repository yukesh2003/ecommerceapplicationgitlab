package com.ecommerce.controller.Filter;
import com.ecommerce.model.user.User;
import com.ecommerce.model.util.Enums;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DeliveryAgentFilter implements Filter {
    public void init(FilterConfig config)  {
    }

    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response=(HttpServletResponse) resp;
        try
        {
            if (request.getSession(false).getAttribute("user") != null)
            {
                if (((User) request.getSession(false).getAttribute("user")).getUserType() != Enums.UserType.DELIVERY_AGENT) {
                    request.getRequestDispatcher("accessError.jsp").forward(request, response);
                } else {
                    chain.doFilter(request, response);
                }
            }
            else
            {
                response.sendRedirect("login.jsp");
            }
        }
        catch (NullPointerException e)
        {
            response.sendRedirect("login.jsp");
            Logger.getAnonymousLogger().log(Level.SEVERE,e.getMessage());
        }

    }
}
