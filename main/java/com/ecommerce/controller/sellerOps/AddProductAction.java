package com.ecommerce.controller.sellerOps;
import com.ecommerce.model.DbOperations.BeanInstance;
import com.ecommerce.model.DbOperations.ProductDAOImpl;
import com.ecommerce.model.product.Product;
import com.ecommerce.model.product.ProductDescription;
import com.ecommerce.model.user.User;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.*;
import java.sql.SQLException;
@MultipartConfig(maxFileSize = 16177215)
public class AddProductAction extends ActionSupport {
    private File image;
    private String brand,category,name,price,color,size,idealFor;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getIdealFor() {
        return idealFor;
    }

    public void setIdealFor(String idealFor) {
        this.idealFor = idealFor;
    }

    public File getImage() {
        return image;
    }

    public void setImage(File image) {
        this.image = image;
    }

    @Override
    public String execute() throws IOException, ServletException {
        HttpServletRequest request= ServletActionContext.getRequest();
        HttpServletResponse response=ServletActionContext.getResponse();
        int sellerId = ((User)request.getSession().getAttribute("user")).getUserId();
//----------------------Image Upload----------------------------------

        String imagePath="/Users/yukesh-pt6737/Desktop/ecommerceimages/"+name+brand+category+sellerId+".jpeg";
        try
        {
            FileOutputStream fos = new FileOutputStream(imagePath);
            InputStream is = new FileInputStream(image);
            byte[] data = new byte[is.available()];
            is.read(data);
            fos.write(data);
            fos.flush();
            fos.close();
        }
        catch(Exception e)
        {
            response.getWriter().println(e.getMessage());
        }

//------------------------------------------
            Product product = new Product(sellerId, category, brand);
            product.setImagePath(imagePath);
            product.setName(name);
            product.setPrice(Integer.parseInt(price));
            ProductDescription productDescription = new ProductDescription();
            productDescription.setSize(Integer.parseInt(size));
            productDescription.setColor(color);
            productDescription.setCustomerGroup(idealFor);
            product.setDescription(productDescription);
            boolean addProductStatus;
            PrintWriter out = response.getWriter();
            try {
                addProductStatus = BeanInstance.getProductDAOType2().addProduct(product);
                if (addProductStatus) {
                    addActionMessage("Product Added Successfully");
                    return SUCCESS;
                } else {
                    out.println("<script type=\"text/javascript\" s>");
                    out.println("alert('something went wrong..');");
                    out.println("</script>");
                    return ERROR;

                }
            } catch (SQLException e) {
                out.println("<script type=\"text/javascript\" s>");
                out.println("alert('something went wrong..');");
                out.println("</script>");
                return ERROR;
            }
        }
    }




