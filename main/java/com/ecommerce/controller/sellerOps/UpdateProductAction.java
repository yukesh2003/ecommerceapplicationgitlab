package com.ecommerce.controller.sellerOps;
import com.ecommerce.model.DbOperations.BeanInstance;
import com.ecommerce.model.DbOperations.ProductDAOImpl;
import com.ecommerce.model.product.Product;
import com.ecommerce.model.product.ProductDescription;
import com.ecommerce.model.user.User;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UpdateProductAction extends ActionSupport {
    private String brand,category,name,price,color,size,idealFor,productId;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getIdealFor() {
        return idealFor;
    }

    public void setIdealFor(String idealFor) {
        this.idealFor = idealFor;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Override
    public String execute() throws ServletException, IOException {
        HttpServletRequest request=ServletActionContext.getRequest();
        int sellerId=((User)request.getSession().getAttribute("user")).getUserId();

        Product product=new Product(sellerId,category,brand);
        product.setProductId(Integer.parseInt(productId));
        product.setName(name);
        product.setPrice(Double.parseDouble(price));
        ProductDescription description= new ProductDescription();
        description.setSize(Integer.parseInt(size));
        description.setColor(color);
        description.setCustomerGroup(idealFor);
        product.setDescription(description);
        try
        {
            boolean status= BeanInstance.getProductDAOType2().updateProduct(product);
            if(status)
            {
                addActionMessage("Product Updated Successfully");
                return SUCCESS;
            }
            else
            {
               addActionMessage("cannot edit product");
               return ERROR;
            }
        }
        catch (SQLException e)
        {
            addActionMessage("cannot edit product");
            Logger.getAnonymousLogger().log(Level.SEVERE,e.getMessage());
            return ERROR;
        }
    }
}
