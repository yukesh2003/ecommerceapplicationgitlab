package com.ecommerce.controller.sellerOps;
import com.ecommerce.model.DbOperations.BeanInstance;
import com.ecommerce.model.DbOperations.ProductDAOImpl;
import com.opensymphony.xwork2.ActionSupport;
import javax.servlet.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
public class RemoveProductAction extends ActionSupport {
    private String productId;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Override
    public String execute() throws ServletException, IOException {
        try
        {
            boolean status= BeanInstance.getProductDAOType2().removeProduct(Integer.parseInt(productId));
            if(status)
            {
               addActionMessage("Product removed SuccessFully");
               return SUCCESS;
            }
            else {
                addActionMessage("cannot remove product");
                return ERROR;
            }
        }
        catch (SQLException e) {
            addActionMessage("cannot remove product");
            Logger.getAnonymousLogger().log(Level.SEVERE,e.getMessage());
            return ERROR;
        }

    }
}
