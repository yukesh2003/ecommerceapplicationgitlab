package com.ecommerce.controller;
import com.ecommerce.model.DbOperations.BeanInstance;
import com.ecommerce.model.user.BankAccount;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

public class RegisterBankAction extends ActionSupport {
    private String userId,accountNo,ifsc,bankName;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getIfsc() {
        return ifsc;
    }

    public void setIfsc(String ifsc) {
        this.ifsc = ifsc;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Override
    public String execute() throws ServletException, IOException {
            HttpServletResponse response= ServletActionContext.getResponse();
            BankAccount bankAccount=new BankAccount();
            bankAccount.setBank(bankName);
            bankAccount.setIfsc(ifsc);
            bankAccount.setAccountNo(accountNo);
            bankAccount.setUserId(Integer.parseInt(userId));
            boolean registrationStatus;
            PrintWriter out= response.getWriter();
            response.setContentType("text/html");
        try
        {
            registrationStatus= BeanInstance.getUserDAOType2().registerBank(bankAccount);
            if(registrationStatus)
            {
                out.println("<script type=\"text/javascript\" s>");
                out.println("alert('Bank Details Added Successfully');");
                out.println("</script>");
                return SUCCESS;
            }
            else
            {
                out.println("<script type=\"text/javascript\" s>");
                out.println("alert('can't add bank details...!   Something Went Wrong');");
                out.println("</script>");
                return ERROR;
            }
        } catch (SQLException e) {
            response.getWriter().println(e.getMessage());
            return ERROR;
        }
    }
}
